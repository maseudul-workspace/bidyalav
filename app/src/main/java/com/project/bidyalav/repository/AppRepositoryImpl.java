package com.project.bidyalav.repository;

import android.util.Log;

import com.google.gson.Gson;
import com.project.bidyalav.domain.models.AppearedExamWrapper;
import com.project.bidyalav.domain.models.BidyalavExamList;
import com.project.bidyalav.domain.models.BidyalavExamListWrapper;
import com.project.bidyalav.domain.models.ClassListWrapper;
import com.project.bidyalav.domain.models.CommonResponse;
import com.project.bidyalav.domain.models.ExamDetails;
import com.project.bidyalav.domain.models.ExamDetailsWrapper;
import com.project.bidyalav.domain.models.ExamListWrapper;
import com.project.bidyalav.domain.models.MainData;
import com.project.bidyalav.domain.models.MainDataWrapper;
import com.project.bidyalav.domain.models.OrganizationListWrapper;
import com.project.bidyalav.domain.models.PdfListWrapper;
import com.project.bidyalav.domain.models.RegistrationResponseWrapper;
import com.project.bidyalav.domain.models.UserInfoWrapper;
import com.project.bidyalav.domain.models.VideoListWrapper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.Header;
import retrofit2.http.Path;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = ApiClient.createService(AppRepository.class);
    }

    public UserInfoWrapper checkLogin(String email, String password) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkLogin(email, password);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return userInfoWrapper;
    }

    public MainDataWrapper fetchMainData(String apiToken, int userId) {
        Log.e("LogMsg", "ApiToken: " + apiToken);
        Log.e("LogMsg", "User Id: " + userId);
        MainDataWrapper mainDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchMainData("Bearer " + apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    mainDataWrapper = null;
                }else{
                    mainDataWrapper = gson.fromJson(responseBody, MainDataWrapper.class);
                }
            } else {
                mainDataWrapper = null;
            }
        }catch (Exception e){
            mainDataWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return mainDataWrapper;
    }

    public ExamListWrapper fetchExamList(String apiToken, int userId, int subjectId) {
        Log.e("LogMsg", "SubjectId: " + subjectId);
        ExamListWrapper examListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchExamList("Bearer " + apiToken, userId, subjectId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    examListWrapper = null;
                }else{
                    examListWrapper = gson.fromJson(responseBody, ExamListWrapper.class);
                }
            } else {
                examListWrapper = null;
            }
        }catch (Exception e){
            examListWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
         return examListWrapper;
    }

    public PdfListWrapper fetchPdfList(String apiToken, int userId, int subjectId) {
        PdfListWrapper pdfListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchPdfList("Bearer " + apiToken, userId, subjectId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    pdfListWrapper = null;
                }else{
                        pdfListWrapper = gson.fromJson(responseBody, PdfListWrapper.class);
                }
            } else {
                pdfListWrapper = null;
            }
        }catch (Exception e){
            pdfListWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return pdfListWrapper;
    }

    public UserInfoWrapper fetchUserProfile(String apiToken, int userId) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchUserProfile("Bearer " + apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return userInfoWrapper;
    }

    public CommonResponse updateProfile(String apiToken,
                                        int userId,
                                        String name,
                                        String fathersName,
                                        String mobile,
                                        String dob,
                                        String address,
                                        String state,
                                        String city,
                                        String pin
                                        ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateUser("Bearer " + apiToken, userId, name, fathersName, mobile, dob, address, state, city, pin);

            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

    public ExamDetailsWrapper startExam(String apiToken, int userId, int examId) {
        ExamDetailsWrapper examDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> start = mRepository.startExam("Bearer " + apiToken, userId, examId);

            Response<ResponseBody> response = start.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    examDetailsWrapper = null;
                }else{
                    examDetailsWrapper = gson.fromJson(responseBody, ExamDetailsWrapper.class);
                }
            } else {
                examDetailsWrapper = null;
            }
        }catch (Exception e){
            examDetailsWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return examDetailsWrapper;
    }

    public CommonResponse submitAnswer( String authorization,
                                        int studentExamId,
                                        int questionId,
                                        int answerId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> submit = mRepository.submitAnswer("Bearer " + authorization, studentExamId, questionId, answerId);

            Response<ResponseBody> response = submit.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;

    }

    public CommonResponse endExam( String authorization,
                                        int studentExamId,
                                        int questionId,
                                        int answerId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> submit = mRepository.endExam("Bearer " + authorization, studentExamId, questionId, answerId);

            Response<ResponseBody> response = submit.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

    public BidyalavExamListWrapper fetchBidyalavExamList(String apiToken, int userId) {
        BidyalavExamListWrapper examListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchBidyalavExamList("Bearer " + apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    examListWrapper = null;
                }else{
                    examListWrapper = gson.fromJson(responseBody, BidyalavExamListWrapper.class);
                }
            } else {
                examListWrapper = null;
            }
        }catch (Exception e){
            examListWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return examListWrapper;
    }

    public VideoListWrapper fetchVideoList(String apiToken, int userId, int subjectId) {
        VideoListWrapper videoListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchVideoList("Bearer " + apiToken, userId, subjectId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    videoListWrapper = null;
                }else{
                    videoListWrapper = gson.fromJson(responseBody, VideoListWrapper.class);
                }
            } else {
                videoListWrapper = null;
            }
        }catch (Exception e){
            videoListWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return videoListWrapper;
    }

    public CommonResponse submitBidyalavAnswer( String authorization,
                                        int studentExamId,
                                        int questionId,
                                        int answerId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> submit = mRepository.submitBidyalavAnswer("Bearer " + authorization, studentExamId, questionId, answerId);

            Response<ResponseBody> response = submit.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;

    }

    public ExamDetailsWrapper startBidyalavExam(String apiToken, int userId, int examId) {
        ExamDetailsWrapper examDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> start = mRepository.startBidyalavExam("Bearer " + apiToken, userId, examId);

            Response<ResponseBody> response = start.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    examDetailsWrapper = null;
                }else{
                    examDetailsWrapper = gson.fromJson(responseBody, ExamDetailsWrapper.class);
                }
            } else {
                examDetailsWrapper = null;
            }
        }catch (Exception e){
            examDetailsWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return examDetailsWrapper;
    }

    public CommonResponse endBidyalavExam( String authorization,
                                           int studentExamId,
                                           int questionId,
                                           int answerId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> submit = mRepository.endBidyalavExam("Bearer " + authorization, studentExamId, questionId, answerId);

            Response<ResponseBody> response = submit.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

    public AppearedExamWrapper fetchAppearedExams(String authorization, int userId) {
        AppearedExamWrapper appearedExamWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchAppearedExams("Bearer " + authorization, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    appearedExamWrapper = null;
                }else{
                    appearedExamWrapper = gson.fromJson(responseBody, AppearedExamWrapper.class);
                }
            } else {
                appearedExamWrapper = null;
            }
        }catch (Exception e){
            appearedExamWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return  appearedExamWrapper;
    }

    public AppearedExamWrapper fetchBidyalavAppearedExams(String authorization, int userId) {
        AppearedExamWrapper appearedExamWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchBidyalavAppearedExams("Bearer " + authorization, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    appearedExamWrapper = null;
                }else{
                    appearedExamWrapper = gson.fromJson(responseBody, AppearedExamWrapper.class);
                }
            } else {
                appearedExamWrapper = null;
            }
        }catch (Exception e){
            appearedExamWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return  appearedExamWrapper;
    }

    public RegistrationResponseWrapper registerUser(String name,
                                                    String fathersName,
                                                    String email,
                                                    String mobile,
                                                    String dob,
                                                    String address,
                                                    String state,
                                                    String city,
                                                    String pin) {
        RegistrationResponseWrapper registrationResponseWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> register = mRepository.registerUser(name, fathersName, email, mobile, dob, address, state, city, pin);

            Response<ResponseBody> response = register.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    registrationResponseWrapper = null;
                }else{
                    registrationResponseWrapper = gson.fromJson(responseBody, RegistrationResponseWrapper.class);
                }
            } else {
                registrationResponseWrapper = null;
            }
        }catch (Exception e){
            registrationResponseWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return registrationResponseWrapper;
    }

    public ClassListWrapper fetchClassList(String authorization, int orgId) {
        ClassListWrapper classListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchClassList("Bearer " + authorization, orgId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    classListWrapper = null;
                }else{
                    classListWrapper = gson.fromJson(responseBody, ClassListWrapper.class);
                }
            } else {
                classListWrapper = null;
            }
        }catch (Exception e){
            classListWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return  classListWrapper;
    }

    public OrganizationListWrapper fetchOrganizationList(String authorization, int userId) {
        OrganizationListWrapper organizationListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchOrganizationList("Bearer " + authorization, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    organizationListWrapper = null;
                }else{
                    organizationListWrapper = gson.fromJson(responseBody, OrganizationListWrapper.class);
                }
            } else {
                organizationListWrapper = null;
            }
        }catch (Exception e){
            organizationListWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return  organizationListWrapper;
    }

}
