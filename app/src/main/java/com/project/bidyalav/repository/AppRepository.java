package com.project.bidyalav.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AppRepository {

    @POST("user/login")
    @FormUrlEncoded
    Call<ResponseBody> checkLogin(@Field("email") String email,
                                  @Field("password") String password
    );

    @GET("user/dashboard/{user_id}")
    Call<ResponseBody> fetchMainData(@Header("Authorization") String authorization,
                                     @Path("user_id") int userId);

    @GET("exam/list/{subject_id}/{user_id}")
    Call<ResponseBody> fetchExamList(@Header("Authorization") String authorization,
                                     @Path("user_id") int userId,
                                     @Path("subject_id") int subjectId);

    @GET("subject/file/{user_id}/{subject_id}")
    Call<ResponseBody> fetchPdfList(@Header("Authorization") String authorization,
                                    @Path("user_id") int userId,
                                    @Path("subject_id") int subjectId);

    @GET("user/profile/{user_id}")
    Call<ResponseBody> fetchUserProfile(@Header("Authorization") String authorization,
                                        @Path("user_id") int userId);

    @POST("user/update")
    @FormUrlEncoded
    Call<ResponseBody> updateUser(@Header("Authorization") String authorization,
                                  @Field("id") int userId,
                                  @Field("name") String name,
                                  @Field("father_name") String fathersName,
                                  @Field("mobile") String mobile,
                                  @Field("dob") String dob,
                                  @Field("address") String address,
                                  @Field("state") String state,
                                  @Field("city") String city,
                                  @Field("pin") String pin
                                  );

    @GET("exam/start/{exam_id}/{user_id}")
    Call<ResponseBody> startExam(@Header("Authorization") String authorization,
                                 @Path("user_id") int userId,
                                 @Path("exam_id") int examId
                                 );

    @GET("submit/question/{stuednt_exam_id}/{question_id}/{answer_id}")
    Call<ResponseBody> submitAnswer(    @Header("Authorization") String authorization,
                                        @Path("stuednt_exam_id") int studentExamId,
                                        @Path("question_id") int questionId,
                                        @Path("answer_id") int answerId
    );

    @GET("end/exam/{stuednt_exam_id}/{question_id}/{answer_id}")
    Call<ResponseBody> endExam(    @Header("Authorization") String authorization,
                                   @Path("stuednt_exam_id") int studentExamId,
                                   @Path("question_id") int questionId,
                                   @Path("answer_id") int answerId
    );

    @GET("bidya/exam/list/{user_id}")
    Call<ResponseBody> fetchBidyalavExamList(@Header("Authorization") String authorization,
                                             @Path("user_id") int userId);

    @GET("subject/video/{user_id}/{subject_id}")
    Call<ResponseBody> fetchVideoList(@Header("Authorization") String authorization,
                                      @Path("user_id") int userId,
                                      @Path("subject_id") int subjectId
                                      );

    @GET("bidya/submit/question/{stuednt_exam_id}/{question_id}/{answer_id}")
    Call<ResponseBody> submitBidyalavAnswer(    @Header("Authorization") String authorization,
                                                @Path("stuednt_exam_id") int studentExamId,
                                                @Path("question_id") int questionId,
                                                @Path("answer_id") int answerId
    );

    @GET("bidya/exam/start/{exam_id}/{user_id}")
    Call<ResponseBody> startBidyalavExam(@Header("Authorization") String authorization,
                                         @Path("user_id") int userId,
                                         @Path("exam_id") int examId
    );

    @GET("end/exam/{stuednt_exam_id}/{question_id}/{answer_id}")
    Call<ResponseBody> endBidyalavExam(    @Header("Authorization") String authorization,
                                           @Path("stuednt_exam_id") int studentExamId,
                                           @Path("question_id") int questionId,
                                           @Path("answer_id") int answerId
    );

    @GET("my/exam/list/{user_id}")
    Call<ResponseBody> fetchAppearedExams(@Header("Authorization") String authorization,
                                         @Path("user_id") int userId
    );

    @GET("my/bidya/exam/list/{user_id}")
    Call<ResponseBody> fetchBidyalavAppearedExams(  @Header("Authorization") String authorization,
                                                    @Path("user_id") int userId
    );

    @POST("user/reg/request")
    @FormUrlEncoded
    Call<ResponseBody> registerUser(  @Field("name") String name,
                                      @Field("father_name") String fathersName,
                                      @Field("email") String email,
                                      @Field("mobile") String mobile,
                                      @Field("dob") String dob,
                                      @Field("address") String address,
                                      @Field("state") String state,
                                      @Field("city") String city,
                                      @Field("pin") String pin
    );

    @GET("class/list/{org_id}")
    Call<ResponseBody> fetchClassList(  @Header("Authorization") String authorization,
                                        @Path("org_id") int orgId
    );

    @GET("org/list/{user_id}")
    Call<ResponseBody> fetchOrganizationList(  @Header("Authorization") String authorization,
                                        @Path("user_id") int userId
    );

}
