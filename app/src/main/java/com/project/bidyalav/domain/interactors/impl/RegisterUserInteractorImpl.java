package com.project.bidyalav.domain.interactors.impl;

import android.content.Context;

import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.RegisterUserInteractor;
import com.project.bidyalav.domain.interactors.base.AbstractInteractor;
import com.project.bidyalav.domain.models.CommonResponse;
import com.project.bidyalav.domain.models.RegistrationResponseWrapper;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.repository.AppRepositoryImpl;

public class RegisterUserInteractorImpl extends AbstractInteractor implements RegisterUserInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String name;
    String fathersName;
    String email;
    String mobile;
    String dob;
    String address;
    String state;
    String city;
    String pin;

    public RegisterUserInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String name, String fathersName, String email, String mobile, String dob, String address, String state, String city, String pin) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.name = name;
        this.fathersName = fathersName;
        this.email = email;
        this.mobile = mobile;
        this.dob = dob;
        this.address = address;
        this.state = state;
        this.city = city;
        this.pin = pin;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterFail(errorMsg);
            }
        });
    }

    private void postMessage(UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final RegistrationResponseWrapper registrationResponseWrapper = mRepository.registerUser(name, fathersName, email, mobile, dob, address, state, city, pin);
        if (registrationResponseWrapper == null) {
            notifyError("Slow Internet Connection");
        } else if(!registrationResponseWrapper.status) {
            notifyError(registrationResponseWrapper.message);
        } else {
            postMessage(registrationResponseWrapper.userInfo);
        }
    }
}
