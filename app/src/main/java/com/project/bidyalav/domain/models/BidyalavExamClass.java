package com.project.bidyalav.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BidyalavExamClass {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("exam_id")
    @Expose
    public int examId;

    @SerializedName("class_id")
    @Expose
    public int classId;

    @SerializedName("class")
    @Expose
    public Class bidyalavClass;

}
