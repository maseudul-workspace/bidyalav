package com.project.bidyalav.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PDF {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("org_id")
    @Expose
    public int orgId;

    @SerializedName("subject_id")
    @Expose
    public int subjectId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("pdf_file")
    @Expose
    public String pdfFile;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("status")
    @Expose
    public int status;

}
