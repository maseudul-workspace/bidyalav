package com.project.bidyalav.domain.interactors;

import com.project.bidyalav.domain.models.ExamDetails;

public interface StartExamInteractor {
    interface Callback {
        void onGettingExamDetailsSuccess(ExamDetails examDetails);
        void onGettingExamDetailsFail(int loginError, String errorMsg);
    }
}
