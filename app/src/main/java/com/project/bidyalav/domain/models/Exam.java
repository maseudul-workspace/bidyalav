package com.project.bidyalav.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Exam {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("class_id")
    @Expose
    public int classId;

    @SerializedName("org_id")
    @Expose
    public int orgId;

    @SerializedName("subject_id")
    @Expose
    public int subjectId;

    @SerializedName("exam_type")
    @Expose
    public int examType;

    @SerializedName("name")
    @Expose
    public String examName;

    @SerializedName("start_date")
    @Expose
    public String startDate;

    @SerializedName("end_date")
    @Expose
    public String endDate;

    @SerializedName("exam_status")
    @Expose
    public int examStatus;

    @SerializedName("total_mark")
    @Expose
    public int totalMarks;

    @SerializedName("pass_mark")
    @Expose
    public int passMark;

    @SerializedName("duration")
    @Expose
    public int duration;

}
