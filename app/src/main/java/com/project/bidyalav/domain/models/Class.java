package com.project.bidyalav.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Class {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("stream_id")
    @Expose
    public int streamId;

    @SerializedName("org_id")
    @Expose
    public int orgId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("subject")
    @Expose
    public Subject[] subjects;

}
