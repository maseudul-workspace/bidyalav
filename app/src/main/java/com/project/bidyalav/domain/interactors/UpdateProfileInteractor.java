package com.project.bidyalav.domain.interactors;

public interface UpdateProfileInteractor {
    interface Callback {
        void onUserUpdateSuccess();
        void onUserUpdateFail(String errorMsg, int loginError);
    }
}
