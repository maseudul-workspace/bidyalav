package com.project.bidyalav.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoList {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("org_id")
    @Expose
    public int orgId;

    @SerializedName("subject_id")
    @Expose
    public int subjectId;

    @SerializedName("video_id")
    @Expose
    public String videoId;

    @SerializedName("status")
    @Expose
    public int status;

}
