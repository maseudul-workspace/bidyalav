package com.project.bidyalav.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subject {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("class_id")
    @Expose
    public int classId;

    @SerializedName("org_id")
    @Expose
    public int orgId;

    @SerializedName("name")
    @Expose
    public String name;

}
