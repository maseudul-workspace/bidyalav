package com.project.bidyalav.domain.interactors;

public interface SubmitBidyalavAnswerInteractor {
    interface Callback {
        void onAnswerSubmitSuccess();
        void onAnswerSubmitFail(int loginError, String errorMsg);
    }
}
