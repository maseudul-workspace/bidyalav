package com.project.bidyalav.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppearedExam {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("org_id")
    @Expose
    public int ordId;

    @SerializedName("student_id")
    @Expose
    public int studentId;

    @SerializedName("exam_id")
    @Expose
    public int examId;

    @SerializedName("marks_obtain")
    @Expose
    public String marksObtained;

    @SerializedName("exam_status")
    @Expose
    public int examStatus;

    @SerializedName("exam")
    @Expose
    public Exam exam;

}
