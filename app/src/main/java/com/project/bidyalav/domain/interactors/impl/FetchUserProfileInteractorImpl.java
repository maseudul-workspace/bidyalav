package com.project.bidyalav.domain.interactors.impl;

import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchUserProfileInteractor;
import com.project.bidyalav.domain.interactors.base.AbstractInteractor;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.domain.models.UserInfoWrapper;
import com.project.bidyalav.repository.AppRepositoryImpl;

public class FetchUserProfileInteractorImpl extends AbstractInteractor implements FetchUserProfileInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchUserProfileInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserProfileFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserProfileSuccess(userInfo);
            }
        });
    }


    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.fetchUserProfile(apiToken, userId);
        if (userInfoWrapper == null) {
            notifyError("Slow Internet Connection", 0);
        } else if (!userInfoWrapper.status) {
            notifyError(userInfoWrapper.message, userInfoWrapper.loginError);
        } else {
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
