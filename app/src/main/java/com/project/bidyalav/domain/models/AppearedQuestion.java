package com.project.bidyalav.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppearedQuestion {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("student_exam_id")
    @Expose
    public int studentExamId;

    @SerializedName("question_id")
    @Expose
    public int questionId;

    @SerializedName("answer_id")
    @Expose
    public int answerId;

    @SerializedName("is_correct")
    @Expose
    public int isCorrect;

}
