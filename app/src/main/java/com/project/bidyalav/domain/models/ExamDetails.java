package com.project.bidyalav.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExamDetails {

    @SerializedName("exam_status")
    @Expose
    public int examStatus;

    @SerializedName("student_exam_id")
    @Expose
    public int studentExamId;

    @SerializedName("questions")
    @Expose
    public Question[] questions;

    @SerializedName("appeared_question")
    @Expose
    public AppearedQuestion[] appearedQuestions;

}
