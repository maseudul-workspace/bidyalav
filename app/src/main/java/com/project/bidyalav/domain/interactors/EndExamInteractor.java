package com.project.bidyalav.domain.interactors;

public interface EndExamInteractor {
    interface Callback {
        void onEndExamSuccess();
        void onEndExamFail(String errorMsg, int loginError);
    }
}
