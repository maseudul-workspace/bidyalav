package com.project.bidyalav.domain.interactors;

import com.project.bidyalav.domain.models.AppearedExam;

public interface FetchBidyalavAppearedExamInteractor {
    interface Callback {
        void onGettingBidyalavAppearedExamSuccess(AppearedExam[] appearedExams);
        void onGettingBidayalavAppearedExamFail(String errorMsg, int loginError);
    }
}
