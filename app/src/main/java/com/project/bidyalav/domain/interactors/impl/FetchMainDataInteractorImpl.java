package com.project.bidyalav.domain.interactors.impl;

import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchMainDataInteractor;
import com.project.bidyalav.domain.interactors.base.AbstractInteractor;
import com.project.bidyalav.domain.models.MainData;
import com.project.bidyalav.domain.models.MainDataWrapper;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.repository.AppRepository;
import com.project.bidyalav.repository.AppRepositoryImpl;

public class FetchMainDataInteractorImpl extends AbstractInteractor implements FetchMainDataInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchMainDataInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMainDataFailed(errorMsg, loginError);
            }
        });
    }

    private void postMessage(MainData mainData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMainDataSuccess(mainData);
            }
        });
    }

    @Override
    public void run() {
        final MainDataWrapper mainDataWrapper = mRepository.fetchMainData(apiToken, userId);
        if (mainDataWrapper == null) {
            notifyError("Slow Internet Connection", 0);
        } else if (!mainDataWrapper.status) {
            notifyError("Slow Internet Connection", mainDataWrapper.loginError);
        } else {
            postMessage(mainDataWrapper.mainData);
        }
    }
}
