package com.project.bidyalav.domain.interactors.impl;

import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchPdfListInteractor;
import com.project.bidyalav.domain.interactors.base.AbstractInteractor;
import com.project.bidyalav.domain.models.Exam;
import com.project.bidyalav.domain.models.PDF;
import com.project.bidyalav.domain.models.PdfListWrapper;
import com.project.bidyalav.repository.AppRepositoryImpl;

public class FetchPdfListInteractorImpl extends AbstractInteractor implements FetchPdfListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    int subjectId;

    public FetchPdfListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, int subjectId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.subjectId = subjectId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingPdfListFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(PDF[] pdfs){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingPdfListSuccess(pdfs);
            }
        });
    }

    @Override
    public void run() {
        final PdfListWrapper pdfListWrapper = mRepository.fetchPdfList(apiToken, userId, subjectId);
        if (pdfListWrapper == null) {
            notifyError("Slow Internet Connection", 0);
        } else if (!pdfListWrapper.status) {
            notifyError("Slow Internet Connection", pdfListWrapper.loginError);
        } else {
            postMessage(pdfListWrapper.pdfs);
        }
    }
}
