package com.project.bidyalav.domain.interactors;
import com.project.bidyalav.domain.models.UserInfo;

public interface CheckLoginInteractor {
    interface Callback {
        void onCheckLoginSuccess(UserInfo user);
        void onCheckLoginFail(String errorMsg);
    }
}
