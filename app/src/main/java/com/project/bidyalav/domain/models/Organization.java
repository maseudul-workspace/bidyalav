package com.project.bidyalav.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Organization {

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("name")
    @Expose
    public String name;

}
