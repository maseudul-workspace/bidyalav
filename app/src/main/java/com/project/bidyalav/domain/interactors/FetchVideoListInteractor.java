package com.project.bidyalav.domain.interactors;

import com.project.bidyalav.domain.models.VideoList;

public interface FetchVideoListInteractor {
    interface Callback {
        void onGettingVideoListSuccess(VideoList[] videoLists);
        void onGettingVideoListFail(String errorMsg, int loginError);
    }
}
