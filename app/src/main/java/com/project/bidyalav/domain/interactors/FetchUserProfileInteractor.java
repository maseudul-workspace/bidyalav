package com.project.bidyalav.domain.interactors;

import com.project.bidyalav.domain.models.UserInfo;

public interface FetchUserProfileInteractor {
    interface Callback {
        void onGettingUserProfileSuccess(UserInfo userInfo);
        void onGettingUserProfileFail(String errorMsg, int loginError);
    }
}
