package com.project.bidyalav.domain.interactors;

import com.project.bidyalav.domain.models.MainData;

public interface FetchMainDataInteractor {
    interface Callback {
        void onGettingMainDataSuccess(MainData mainData);
        void onGettingMainDataFailed(String errorMsg, int loginError);
    }
}
