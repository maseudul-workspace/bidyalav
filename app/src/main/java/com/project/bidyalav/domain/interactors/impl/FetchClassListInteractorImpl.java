package com.project.bidyalav.domain.interactors.impl;

import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchClassListInteractor;
import com.project.bidyalav.domain.interactors.base.AbstractInteractor;
import com.project.bidyalav.domain.models.Class;
import com.project.bidyalav.domain.models.ClassListWrapper;
import com.project.bidyalav.domain.models.Exam;
import com.project.bidyalav.repository.AppRepositoryImpl;

public class FetchClassListInteractorImpl extends AbstractInteractor implements FetchClassListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int orgId;

    public FetchClassListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int orgId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.orgId = orgId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingClassListFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(Class[] classes){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingClassListSuccess(classes);
            }
        });
    }

    @Override
    public void run() {
        ClassListWrapper classListWrapper = mRepository.fetchClassList(apiToken, orgId);
        if (classListWrapper == null) {
            notifyError("Slow Internet Connection", 0);
        } else if (!classListWrapper.status) {
            notifyError("Slow Internet Connection", classListWrapper.loginError);
        } else {
            postMessage(classListWrapper.classes);
        }
    }
}
