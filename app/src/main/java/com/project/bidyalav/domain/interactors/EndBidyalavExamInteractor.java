package com.project.bidyalav.domain.interactors;

public interface EndBidyalavExamInteractor {
    interface Callback {
        void onEndExamSuccess();
        void onEndExamFail(String errorMsg, int loginError);
    }
}
