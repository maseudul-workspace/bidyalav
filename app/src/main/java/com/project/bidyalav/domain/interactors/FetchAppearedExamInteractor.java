package com.project.bidyalav.domain.interactors;

import com.project.bidyalav.domain.models.AppearedExam;

public interface FetchAppearedExamInteractor {
    interface Callback {
        void onGettingAppearedExamSuccess(AppearedExam[] appearedExams);
        void onGettingAppearedExamFail(String errorMsg, int loginError);
    }
}
