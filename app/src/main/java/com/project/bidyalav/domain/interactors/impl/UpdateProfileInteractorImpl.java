package com.project.bidyalav.domain.interactors.impl;

import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.UpdateProfileInteractor;
import com.project.bidyalav.domain.interactors.base.AbstractInteractor;
import com.project.bidyalav.domain.models.CommonResponse;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.repository.AppRepositoryImpl;

public class UpdateProfileInteractorImpl extends AbstractInteractor implements UpdateProfileInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    String name;
    String fathersName;
    String mobile;
    String dob;
    String address;
    String state;
    String city;
    String pin;

    public UpdateProfileInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, String name, String fathersName, String mobile, String dob, String address, String state, String city, String pin) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.name = name;
        this.fathersName = fathersName;
        this.mobile = mobile;
        this.dob = dob;
        this.address = address;
        this.state = state;
        this.city = city;
        this.pin = pin;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUserUpdateFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUserUpdateSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.updateProfile(apiToken, userId, name, fathersName, mobile, dob, address, state, city, pin);
        if (commonResponse == null) {
            notifyError("Slow Internet Connection", 0);
        } else if(!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.loginError);
        } else {
            postMessage();
        }
    }
}
