package com.project.bidyalav.domain.interactors;

import com.project.bidyalav.domain.models.Class;

public interface FetchClassListInteractor {
    interface Callback {
        void onGettingClassListSuccess(Class[] classes);
        void onGettingClassListFail(String errorMsg, int loginError);
    }
}
