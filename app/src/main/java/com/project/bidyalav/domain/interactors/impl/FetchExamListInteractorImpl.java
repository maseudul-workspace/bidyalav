package com.project.bidyalav.domain.interactors.impl;

import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchExamListInteractor;
import com.project.bidyalav.domain.interactors.base.AbstractInteractor;
import com.project.bidyalav.domain.models.Exam;
import com.project.bidyalav.domain.models.ExamListWrapper;
import com.project.bidyalav.domain.models.MainData;
import com.project.bidyalav.presentation.presenters.base.AbstractPresenter;
import com.project.bidyalav.repository.AppRepositoryImpl;

public class FetchExamListInteractorImpl extends AbstractInteractor implements FetchExamListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    int subjectId;

    public FetchExamListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, int subjectId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.subjectId = subjectId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingExamListFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(Exam[] exams){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingExamListSuccess(exams);
            }
        });
    }

    @Override
    public void run() {
        final ExamListWrapper examListWrapper = mRepository.fetchExamList(apiToken, userId, subjectId);
        if (examListWrapper == null) {
            notifyError("Slow Internet Connection", 0);
        } else if (!examListWrapper.status) {
            notifyError("Slow Internet Connection", examListWrapper.loginError);
        } else {
            postMessage(examListWrapper.exams);
        }
    }
}
