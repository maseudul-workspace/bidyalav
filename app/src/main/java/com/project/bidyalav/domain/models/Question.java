package com.project.bidyalav.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Question {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("exam_id")
    @Expose
    public int examId;

    @SerializedName("question_type")
    @Expose
    public int questionType;

    @SerializedName("question")
    @Expose
    public String question;

    @SerializedName("correct_answer_id")
    @Expose
    public int correctAnswerId;

    @SerializedName("options")
    @Expose
    public Answer[] answers;

}
