package com.project.bidyalav.domain.interactors.impl;

import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchOrganizationiListInteractor;
import com.project.bidyalav.domain.interactors.base.AbstractInteractor;
import com.project.bidyalav.domain.models.Exam;
import com.project.bidyalav.domain.models.Organization;
import com.project.bidyalav.domain.models.OrganizationListWrapper;
import com.project.bidyalav.repository.AppRepositoryImpl;

public class FetchOrganizationListInteractorImpl extends AbstractInteractor implements FetchOrganizationiListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchOrganizationListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrganizationListFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(Organization[] organizations){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrganizationListSuccess(organizations);
            }
        });
    }

    @Override
    public void run() {
        final OrganizationListWrapper organizationListWrapper = mRepository.fetchOrganizationList(apiToken, userId);
        if (organizationListWrapper == null) {
            notifyError("Slow Internet Connection", 0);
        } else if (!organizationListWrapper.status) {
            notifyError(organizationListWrapper.message, organizationListWrapper.loginError);
        } else {
            postMessage(organizationListWrapper.organizations);
        }
    }
}
