package com.project.bidyalav.domain.interactors;

public interface SubmitAnswerInteractor {
    interface Callback {
        void onAnswerSubmitSuccess();
        void onAnswerSubmitFail(int loginError, String errorMsg);
    }
}
