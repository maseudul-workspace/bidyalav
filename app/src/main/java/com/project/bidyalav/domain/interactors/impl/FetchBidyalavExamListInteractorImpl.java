package com.project.bidyalav.domain.interactors.impl;

import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchBidyalavExamListInteractor;
import com.project.bidyalav.domain.interactors.base.AbstractInteractor;
import com.project.bidyalav.domain.models.BidyalavExamList;
import com.project.bidyalav.domain.models.BidyalavExamListWrapper;
import com.project.bidyalav.domain.models.Exam;
import com.project.bidyalav.domain.models.ExamListWrapper;
import com.project.bidyalav.repository.AppRepositoryImpl;

public class FetchBidyalavExamListInteractorImpl extends AbstractInteractor implements FetchBidyalavExamListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchBidyalavExamListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingBidyalavExamFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(BidyalavExamList[] exams){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingBidyalavExamSuccess(exams);
            }
        });
    }

    @Override
    public void run() {
        BidyalavExamListWrapper examListWrapper = mRepository.fetchBidyalavExamList(apiToken, userId);
        if (examListWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!examListWrapper.status) {
            notifyError(examListWrapper.message, examListWrapper.loginError);
        } else {
            postMessage(examListWrapper.bidyalavExamLists);
        }
    }
}
