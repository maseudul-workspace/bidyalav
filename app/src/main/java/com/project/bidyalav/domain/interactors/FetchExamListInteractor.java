package com.project.bidyalav.domain.interactors;

import com.project.bidyalav.domain.models.Exam;

public interface FetchExamListInteractor {
    interface Callback {
        void onGettingExamListSuccess(Exam[] exams);
        void onGettingExamListFail(String errorMsg, int loginError);
    }
}
