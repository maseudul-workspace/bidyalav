package com.project.bidyalav.domain.interactors;

import com.project.bidyalav.domain.models.UserInfo;

public interface RegisterUserInteractor {
    interface Callback {
        void onRegisterSuccess(UserInfo userInfo);
        void onRegisterFail(String errorMsg);
    }
}
