package com.project.bidyalav.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Answer {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("exam_question_list_id")
    @Expose
    public int examQuestionListId;

    @SerializedName("option")
    @Expose
    public String option;

    @SerializedName("option_type")
    @Expose
    public int optionType;

    public boolean isSelected = false;

}
