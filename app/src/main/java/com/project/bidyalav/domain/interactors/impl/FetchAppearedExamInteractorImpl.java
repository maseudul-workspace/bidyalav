package com.project.bidyalav.domain.interactors.impl;

import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchAppearedExamInteractor;
import com.project.bidyalav.domain.interactors.base.AbstractInteractor;
import com.project.bidyalav.domain.models.AppearedExam;
import com.project.bidyalav.domain.models.AppearedExamWrapper;
import com.project.bidyalav.domain.models.BidyalavExamList;
import com.project.bidyalav.repository.AppRepositoryImpl;

public class FetchAppearedExamInteractorImpl extends AbstractInteractor implements FetchAppearedExamInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchAppearedExamInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAppearedExamFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(AppearedExam[] appearedExams){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAppearedExamSuccess(appearedExams);
            }
        });
    }

    @Override
    public void run() {
        AppearedExamWrapper appearedExamWrapper = mRepository.fetchAppearedExams(apiToken, userId);
        if (appearedExamWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!appearedExamWrapper.status) {
            notifyError(appearedExamWrapper.message, appearedExamWrapper.loginError);
        } else {
            postMessage(appearedExamWrapper.appearedExams);
        }
    }
}
