package com.project.bidyalav.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfo {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("class_id")
    @Expose
    public int classId;

    @SerializedName("student_id")
    @Expose
    public String studentId;

    @SerializedName("org_id")
    @Expose
    public int orgId = 1;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("father_name")
    @Expose
    public String fatherName;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("mobile")
    @Expose
    public String mobile;

    @SerializedName("dob")
    @Expose
    public String dob;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("pin")
    @Expose
    public String pin;

    @SerializedName("api_token")
    @Expose
    public String apiToken;

    @SerializedName("status")
    @Expose
    public int status = 2;

}
