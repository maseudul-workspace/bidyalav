package com.project.bidyalav.domain.interactors;

import com.project.bidyalav.domain.models.BidyalavExamList;
import com.project.bidyalav.domain.models.Exam;

public interface FetchBidyalavExamListInteractor {
    interface Callback {
        void onGettingBidyalavExamSuccess(BidyalavExamList[] exams);
        void onGettingBidyalavExamFail(String errorMsg, int loginError);
    }
}
