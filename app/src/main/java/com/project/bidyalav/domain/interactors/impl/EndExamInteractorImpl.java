package com.project.bidyalav.domain.interactors.impl;

import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.EndExamInteractor;
import com.project.bidyalav.domain.interactors.base.AbstractInteractor;
import com.project.bidyalav.domain.models.CommonResponse;
import com.project.bidyalav.repository.AppRepositoryImpl;

public class EndExamInteractorImpl extends AbstractInteractor implements EndExamInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int studentExamId;
    int questionId;
    int answerId;

    public EndExamInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int studentExamId, int questionId, int answerId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.studentExamId = studentExamId;
        this.questionId = questionId;
        this.answerId = answerId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onEndExamFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onEndExamSuccess();
            }
        });
    }

    @Override
    public void run() {
        CommonResponse commonResponse = mRepository.endExam(apiToken, studentExamId, questionId, answerId);
        if (commonResponse == null) {
            notifyError("Please Check Your Connection", 0);
        } else if (!commonResponse.status) {
            notifyError("Something Went Wrong", commonResponse.loginError);
        } else {
            postMessage();
        }
    }
}
