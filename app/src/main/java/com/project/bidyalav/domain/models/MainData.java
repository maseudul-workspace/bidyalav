package com.project.bidyalav.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainData {

    @SerializedName("org_image")
    @Expose
    public String orgImage;

    @SerializedName("class")
    @Expose
    public Class[] classes;

    @SerializedName("org_images")
    @Expose
    public Organization[] organizations;

}
