package com.project.bidyalav.domain.interactors;

import com.project.bidyalav.domain.models.Organization;

public interface FetchOrganizationiListInteractor {
    interface Callback {
        void onGettingOrganizationListSuccess(Organization[] organizations);
        void onGettingOrganizationListFail(String errorMsg, int loginError);
    }
}
