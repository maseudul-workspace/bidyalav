package com.project.bidyalav.domain.interactors.impl;

import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchVideoListInteractor;
import com.project.bidyalav.domain.interactors.base.AbstractInteractor;
import com.project.bidyalav.domain.models.Exam;
import com.project.bidyalav.domain.models.VideoList;
import com.project.bidyalav.domain.models.VideoListWrapper;
import com.project.bidyalav.repository.AppRepositoryImpl;

public class FetchVideoListInteractorImpl extends AbstractInteractor implements FetchVideoListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    int subjectId;

    public FetchVideoListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, int subjectId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.subjectId = subjectId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingVideoListFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(VideoList[] videoLists){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingVideoListSuccess(videoLists);
            }
        });
    }

    @Override
    public void run() {
        final VideoListWrapper videoListWrapper = mRepository.fetchVideoList(apiToken, userId, subjectId);
        if (videoListWrapper == null) {
            notifyError("Please Check Your Connection", 0);
        } else if (!videoListWrapper.status) {
             notifyError(videoListWrapper.message, videoListWrapper.loginError);
        } else {
            postMessage(videoListWrapper.videoLists);
        }
    }
}
