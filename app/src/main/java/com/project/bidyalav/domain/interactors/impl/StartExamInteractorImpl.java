package com.project.bidyalav.domain.interactors.impl;

import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.StartExamInteractor;
import com.project.bidyalav.domain.interactors.base.AbstractInteractor;
import com.project.bidyalav.domain.models.Exam;
import com.project.bidyalav.domain.models.ExamDetails;
import com.project.bidyalav.domain.models.ExamDetailsWrapper;
import com.project.bidyalav.repository.AppRepositoryImpl;

public class StartExamInteractorImpl extends AbstractInteractor implements StartExamInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    int examId;

    public StartExamInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, int examId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.examId = examId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingExamDetailsFail(loginError, errorMsg);
            }
        });
    }

    private void postMessage(ExamDetails examDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingExamDetailsSuccess(examDetails);
            }
        });
    }

    @Override
    public void run() {
        ExamDetailsWrapper examDetailsWrapper = mRepository.startExam(apiToken, userId, examId);
        if (examDetailsWrapper == null) {
            notifyError("Please Check Your Connection", 0);
        } else if (!examDetailsWrapper.status) {
            notifyError(examDetailsWrapper.message, examDetailsWrapper.loginError);
        } else {
            postMessage(examDetailsWrapper.examDetails);
        }
    }
}
