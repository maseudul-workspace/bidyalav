package com.project.bidyalav.domain.interactors;

import com.project.bidyalav.domain.models.PDF;

public interface FetchPdfListInteractor {
    interface Callback {
        void onGettingPdfListSuccess(PDF[] pdfs);
        void onGettingPdfListFail(String errorMsg, int loginError);
    }
}
