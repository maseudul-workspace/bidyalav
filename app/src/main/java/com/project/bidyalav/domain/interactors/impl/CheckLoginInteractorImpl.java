package com.project.bidyalav.domain.interactors.impl;


import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.CheckLoginInteractor;
import com.project.bidyalav.domain.interactors.base.AbstractInteractor;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.domain.models.UserInfoWrapper;
import com.project.bidyalav.repository.AppRepositoryImpl;

public class CheckLoginInteractorImpl extends AbstractInteractor implements CheckLoginInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String email;
    String password;

    public CheckLoginInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String email, String password) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.email = email;
        this.password = password;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckLoginFail(errorMsg);
            }
        });
    }

    private void postMessage(UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckLoginSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userWrapper = mRepository.checkLogin(email, password);
        if (userWrapper == null) {
            notifyError("Slow Internet Connection");
        } else if (!userWrapper.status) {
            notifyError(userWrapper.message);
        } else {
            postMessage(userWrapper.userInfo);
        }
    }
}
