package com.project.bidyalav.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.executors.impl.ThreadExecutor;
import com.project.bidyalav.presentation.presenters.AppearedExamPresenter;
import com.project.bidyalav.presentation.presenters.impl.AppearedExamPresenterImpl;
import com.project.bidyalav.presentation.ui.adapters.AppearedExamAdapter;
import com.project.bidyalav.threading.MainThreadImpl;

public class AppearedExamActivity extends AppCompatActivity implements AppearedExamPresenter.View {

    @BindView(R.id.recycler_view_appeared_exams)
    RecyclerView recyclerViewAppearedExams;
    AppearedExamPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appeared_exam);
        getSupportActionBar().setTitle("Appeared Exams");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchAppearedExams();
    }

    private void initialisePresenter() {
        mPresenter = new AppearedExamPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAppearedExamsAdapter(AppearedExamAdapter adapter) {
        recyclerViewAppearedExams.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewAppearedExams.setAdapter(adapter);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}