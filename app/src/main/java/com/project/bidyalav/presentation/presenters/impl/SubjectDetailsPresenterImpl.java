package com.project.bidyalav.presentation.presenters.impl;

import android.content.Context;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchExamListInteractor;
import com.project.bidyalav.domain.interactors.FetchPdfListInteractor;
import com.project.bidyalav.domain.interactors.FetchVideoListInteractor;
import com.project.bidyalav.domain.interactors.impl.FetchExamListInteractorImpl;
import com.project.bidyalav.domain.interactors.impl.FetchPdfListInteractorImpl;
import com.project.bidyalav.domain.interactors.impl.FetchVideoListInteractorImpl;
import com.project.bidyalav.domain.models.Exam;
import com.project.bidyalav.domain.models.PDF;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.domain.models.VideoList;
import com.project.bidyalav.presentation.presenters.SubjectDetailsPresenter;
import com.project.bidyalav.presentation.presenters.base.AbstractPresenter;
import com.project.bidyalav.presentation.ui.adapters.ExamsAdapter;
import com.project.bidyalav.presentation.ui.adapters.PdfAdapter;
import com.project.bidyalav.presentation.ui.adapters.VideoListAdapter;
import com.project.bidyalav.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class SubjectDetailsPresenterImpl extends AbstractPresenter implements   SubjectDetailsPresenter,
                                                                                FetchExamListInteractor.Callback,
                                                                                FetchPdfListInteractor.Callback,
                                                                                ExamsAdapter.Callback,
                                                                                PdfAdapter.Callback,
                                                                                FetchVideoListInteractor.Callback,
                                                                                VideoListAdapter.Callback
{

    Context mContext;
    SubjectDetailsPresenter.View mView;
    FetchExamListInteractorImpl fetchExamListInteractor;
    FetchPdfListInteractorImpl fetchPdfListInteractor;
    FetchVideoListInteractorImpl fetchVideoListInteractor;

    public SubjectDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchExamsList(int subjectId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.error(mContext, "Session Expired !! Please Login Again").show();
        } else {
            fetchExamListInteractor = new FetchExamListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id, subjectId);
            fetchExamListInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void fetchPdfList(int subjectId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.error(mContext, "Session Expired !! Please Login Again").show();
        } else {
            fetchPdfListInteractor = new FetchPdfListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id, subjectId);
            fetchPdfListInteractor.execute();
        }
    }

    @Override
    public void fetchVideoList(int subjectId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.error(mContext, "Session Expired !! Please Login Again").show();
        } else {
            fetchVideoListInteractor = new FetchVideoListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id, subjectId);
            fetchVideoListInteractor.execute();
        }
    }

    @Override
    public void onGettingExamListSuccess(Exam[] exams) {
        ExamsAdapter adapter = new ExamsAdapter(mContext, exams, this);
        mView.loadExams(adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingExamListFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onGettingPdfListSuccess(PDF[] pdfs) {
        PdfAdapter adapter = new PdfAdapter(mContext, pdfs, this);
        mView.loadPdfs(adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingPdfListFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onExamClicked(int id) {
        mView.goToExamDetailsActivity(id);
    }

    @Override
    public void onPdfClicked(String pdf) {
        mView.goToViewPdfActivity(pdf);
    }

    @Override
    public void onGettingVideoListSuccess(VideoList[] videoLists) {
        VideoListAdapter videoListAdapter = new VideoListAdapter(mContext,videoLists, this);
        mView.loadVideoList(videoListAdapter);
    }

    @Override
    public void onGettingVideoListFail(String errorMsg, int loginError) {

    }

    @Override
    public void onViewClicked(String videoId) {
        mView.goToVideoPreviewActivity(videoId);
    }
}
