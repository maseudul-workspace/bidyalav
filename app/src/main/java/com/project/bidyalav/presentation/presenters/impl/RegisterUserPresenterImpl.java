package com.project.bidyalav.presentation.presenters.impl;

import android.content.Context;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.RegisterUserInteractor;
import com.project.bidyalav.domain.interactors.impl.RegisterUserInteractorImpl;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.presentation.presenters.RegisterUserPresenter;
import com.project.bidyalav.presentation.presenters.base.AbstractPresenter;
import com.project.bidyalav.repository.AppRepositoryImpl;
import es.dmoral.toasty.Toasty;

public class RegisterUserPresenterImpl extends AbstractPresenter implements RegisterUserPresenter, RegisterUserInteractor.Callback {

    Context mContext;
    RegisterUserPresenter.View mView;
    RegisterUserInteractorImpl registerUserInteractor;

    public RegisterUserPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void registerUser(String name, String fathersName, String email, String mobile, String dob, String address, String state, String city, String pin) {
        registerUserInteractor = new RegisterUserInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, name, fathersName, email, mobile, dob, address, state, city, pin);
        registerUserInteractor.execute();
    }

    @Override
    public void onRegisterSuccess(UserInfo userInfo) {
        mView.hideLoader();
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        mView.onRegistrationSuccess();
    }

    @Override
    public void onRegisterFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

}
