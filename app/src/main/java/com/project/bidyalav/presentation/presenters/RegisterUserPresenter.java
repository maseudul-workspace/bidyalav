package com.project.bidyalav.presentation.presenters;

public interface RegisterUserPresenter {
    void registerUser(String name,
                      String fathersName,
                      String email,
                      String mobile,
                      String dob,
                      String address,
                      String state,
                      String city,
                      String pin);
    interface View {
        void onRegistrationSuccess();
        void showLoader();
        void hideLoader();
    }
}
