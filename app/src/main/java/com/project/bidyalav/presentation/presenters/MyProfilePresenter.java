package com.project.bidyalav.presentation.presenters;

import com.project.bidyalav.domain.models.UserInfo;

public interface MyProfilePresenter {
    void fetchUserProfile();
    void updateProfile(String name,
                       String fathersName,
                       String mobile,
                       String dob,
                       String address,
                       String state,
                       String city,
                       String pin);
    interface View {
        void loadUserProfile(UserInfo userInfo);
        void showLoader();
        void hideLoader();
    }
}
