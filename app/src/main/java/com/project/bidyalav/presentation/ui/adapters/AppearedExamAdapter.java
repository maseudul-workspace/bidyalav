package com.project.bidyalav.presentation.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.R;
import com.project.bidyalav.domain.models.AppearedExam;
import com.project.bidyalav.domain.models.BidyalavExamClass;
import com.project.bidyalav.domain.models.UserInfo;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AppearedExamAdapter extends RecyclerView.Adapter<AppearedExamAdapter.ViewHolder> {

    Context mContext;
    AppearedExam[] appearedExams;

    public AppearedExamAdapter(Context mContext, AppearedExam[] appearedExams) {
        this.mContext = mContext;
        this.appearedExams = appearedExams;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_appeared_exams, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewExamName.setText(appearedExams[position].exam.examName);
        if (appearedExams[position].exam.examStatus == 1) {
            holder.layoutExamNotStarted.setVisibility(View.VISIBLE);
            holder.layoutExamStarted.setVisibility(View.GONE);
            holder.layoutExamFinished.setVisibility(View.GONE);
        } else if (appearedExams[position].exam.examStatus == 2) {
            holder.layoutExamNotStarted.setVisibility(View.GONE);
            holder.layoutExamStarted.setVisibility(View.VISIBLE);
            holder.layoutExamFinished.setVisibility(View.GONE);
        } else {
            holder.layoutExamNotStarted.setVisibility(View.GONE);
            holder.layoutExamStarted.setVisibility(View.GONE);
            holder.layoutExamFinished.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return appearedExams.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_exam_name)
        TextView txtViewExamName;
        @BindView(R.id.layout_exam_finished)
        View layoutExamFinished;
        @BindView(R.id.layout_exam_started)
        View layoutExamStarted;
        @BindView(R.id.layout_exam_not_started)
        View layoutExamNotStarted;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
