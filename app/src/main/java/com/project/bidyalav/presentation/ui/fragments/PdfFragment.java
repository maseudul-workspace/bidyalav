package com.project.bidyalav.presentation.ui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.executors.impl.ThreadExecutor;
import com.project.bidyalav.presentation.presenters.SubjectDetailsPresenter;
import com.project.bidyalav.presentation.presenters.impl.SubjectDetailsPresenterImpl;
import com.project.bidyalav.presentation.ui.activities.PdfViewActivity;
import com.project.bidyalav.presentation.ui.adapters.ExamsAdapter;
import com.project.bidyalav.presentation.ui.adapters.PdfAdapter;
import com.project.bidyalav.presentation.ui.adapters.VideoListAdapter;
import com.project.bidyalav.threading.MainThreadImpl;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PdfFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PdfFragment extends Fragment implements SubjectDetailsPresenter.View {

    @BindView(R.id.recycler_view_pdfs)
    RecyclerView recyclerViewPdf;
    Context mContext;
    int subjectId;
    SubjectDetailsPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    public PdfFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PdfFragment newInstance() {
        PdfFragment fragment = new PdfFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pdf, container, false);
        ButterKnife.bind(this, view);
        subjectId = getArguments().getInt("subjectId");
        mPresenter.fetchPdfList(subjectId);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        setUpProgressDialog();
        initialisePresenter();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initialisePresenter() {
        mPresenter = new SubjectDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(),mContext, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadExams(ExamsAdapter examsAdapter) {

    }

    @Override
    public void loadPdfs(PdfAdapter pdfAdapter) {
        recyclerViewPdf.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewPdf.setAdapter(pdfAdapter);
    }

    @Override
    public void loadVideoList(VideoListAdapter videoListAdapter) {

    }

    @Override
    public void goToViewPdfActivity(String pdf) {
        Intent intent = new Intent(mContext, PdfViewActivity.class);
        intent.putExtra("PDF_URL", mContext.getResources().getString(R.string.base_url) + "view/" + pdf);
        startActivity(intent);
    }

    @Override
    public void goToExamDetailsActivity(int examId) {

    }

    @Override
    public void goToVideoPreviewActivity(String videoId) {

    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

}