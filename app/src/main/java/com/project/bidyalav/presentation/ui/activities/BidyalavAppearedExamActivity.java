package com.project.bidyalav.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.executors.impl.ThreadExecutor;
import com.project.bidyalav.presentation.presenters.BidyalavAppearedExamPresenter;
import com.project.bidyalav.presentation.presenters.impl.AppearedExamPresenterImpl;
import com.project.bidyalav.presentation.presenters.impl.BidyalavAppearedExamPresenterImpl;
import com.project.bidyalav.presentation.ui.adapters.AppearedExamAdapter;
import com.project.bidyalav.threading.MainThreadImpl;

public class BidyalavAppearedExamActivity extends AppCompatActivity implements BidyalavAppearedExamPresenter.View {

    @BindView(R.id.recycler_view_bidyalav_appeared_exams)
    RecyclerView recyclerViewBidyalavAppearedExams;
    BidyalavAppearedExamPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bidyalav_appeared_exam);
        getSupportActionBar().setTitle("Appeared Bidyalav Exam");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchBidyalavAppearedExams();
    }

    private void initialisePresenter() {
        mPresenter = new BidyalavAppearedExamPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadBidyalavAppearedExamsAdapter(AppearedExamAdapter adapter) {
        recyclerViewBidyalavAppearedExams.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewBidyalavAppearedExams.setAdapter(adapter);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}