package com.project.bidyalav.presentation.presenters;

import com.project.bidyalav.presentation.ui.adapters.OrganizationAdapter;

public interface OrganizationListPresenter {
    void fetchOrganizationList();
    interface View {
        void loadAdapter(OrganizationAdapter adapter);
        void showLoader();
        void hideLoader();
    }
}
