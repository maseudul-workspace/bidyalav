package com.project.bidyalav.presentation.presenters;

import com.project.bidyalav.presentation.ui.adapters.AppearedExamAdapter;

public interface BidyalavAppearedExamPresenter {
    void fetchBidyalavAppearedExams();
    interface View {
        void loadBidyalavAppearedExamsAdapter(AppearedExamAdapter adapter);
        void showLoader();
        void hideLoader();
    }
}
