package com.project.bidyalav.presentation.presenters;

import com.project.bidyalav.presentation.ui.adapters.ClassMainAdapter;

public interface ClassListPresenter {
    void fetchClassList();
    interface View {
        void loadAdapter(ClassMainAdapter adapter);
        void goToSubjectDetails(int subjectId);
        void showLoader();
        void hideLoader();
    }
}
