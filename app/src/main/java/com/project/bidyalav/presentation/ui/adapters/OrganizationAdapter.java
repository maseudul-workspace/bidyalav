package com.project.bidyalav.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.models.Organization;
import com.project.bidyalav.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OrganizationAdapter extends RecyclerView.Adapter<OrganizationAdapter.ViewHolder> {

    public interface Callback {

    }

    Context mContext;
    Organization[] organizations;
    Callback mCallback;

    public OrganizationAdapter(Context mContext, Organization[] organizations, Callback mCallback) {
        this.mContext = mContext;
        this.organizations = organizations;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_organization_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewOrganization.setText(organizations[position].name);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewOrganization, mContext.getResources().getString(R.string.base_url) + "org_image/" + organizations[position].image, 20);
    }

    @Override
    public int getItemCount() {
        return organizations.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_organization)
        ImageView imgViewOrganization;
        @BindView(R.id.txt_view_organization)
        TextView txtViewOrganization;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
