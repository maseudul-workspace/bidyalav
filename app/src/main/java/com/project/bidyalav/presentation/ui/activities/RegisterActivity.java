package com.project.bidyalav.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.executors.impl.ThreadExecutor;
import com.project.bidyalav.presentation.presenters.RegisterUserPresenter;
import com.project.bidyalav.presentation.presenters.impl.RegisterUserPresenterImpl;
import com.project.bidyalav.threading.MainThreadImpl;

import java.util.Calendar;

public class RegisterActivity extends AppCompatActivity implements RegisterUserPresenter.View {

    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_city)
    EditText editTextCity;
    @BindView(R.id.edit_text_fathers_name)
    EditText editTextFathersName;
    @BindView(R.id.edit_text_mobile)
    EditText editTextMobile;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    @BindView(R.id.txt_view_dob)
    TextView textViewDob;
    RegisterUserPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    String dob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setTitle("Registration Request");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new RegisterUserPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (    editTextName.getText().toString().trim().isEmpty() ||
                editTextState.getText().toString().trim().isEmpty() ||
                editTextEmail.getText().toString().trim().isEmpty() ||
                editTextPin.getText().toString().trim().isEmpty() ||
                editTextMobile.getText().toString().trim().isEmpty() ||
                editTextFathersName.getText().toString().trim().isEmpty() ||
                editTextCity.getText().toString().trim().isEmpty() ||
                editTextAddress.getText().toString().trim().isEmpty() ||
                dob.isEmpty()
        ) {
            Toasty.warning(this, "All Fields Are Required").show();
        } else {
            mPresenter.registerUser(
                    editTextName.getText().toString(),
                    editTextFathersName.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextMobile.getText().toString(),
                    dob,
                    editTextAddress.getText().toString(),
                    editTextState.getText().toString(),
                    editTextCity.getText().toString(),
                    editTextPin.getText().toString()
            );
            showLoader();
        }
    }

    @OnClick(R.id.view_set_date) void onDOBLayoutClicked() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        textViewDob.setText(dob);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(Color.BLUE);
    }

    @Override
    public void onRegistrationSuccess() {
        Toasty.success(this, "Request Sent Successfully").show();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

}