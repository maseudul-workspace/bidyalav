package com.project.bidyalav.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.project.bidyalav.R;

public class ExamActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam);
        getSupportActionBar().setTitle("Exam");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}