package com.project.bidyalav.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.executors.impl.ThreadExecutor;
import com.project.bidyalav.presentation.presenters.ClassListPresenter;
import com.project.bidyalav.presentation.presenters.impl.ClassListPresenterImpl;
import com.project.bidyalav.presentation.ui.adapters.ClassMainAdapter;
import com.project.bidyalav.threading.MainThreadImpl;

public class ClassListActivity extends AppCompatActivity implements ClassListPresenter.View {

    @BindView(R.id.recycler_view_class_list)
    RecyclerView recyclerViewClassList;
    ClassListPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_list);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("All Classes");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchClassList();
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new ClassListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(ClassMainAdapter adapter) {
        recyclerViewClassList.setAdapter(adapter);
        recyclerViewClassList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void goToSubjectDetails(int subjectId) {
        Intent intent = new Intent(this, SubjectDetailsActivity.class);
        intent.putExtra("subjectId", subjectId);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}