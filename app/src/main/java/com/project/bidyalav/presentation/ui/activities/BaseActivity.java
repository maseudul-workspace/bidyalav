package com.project.bidyalav.presentation.ui.activities;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.navigation.NavigationView;
import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.R;
import com.project.bidyalav.domain.models.UserInfo;

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) @Nullable
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    @BindView(R.id.navigation)
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        getUserinfo();
    }

    protected void inflateContent(@LayoutRes int inflateResID){
        setContentView(R.layout.activity_base);
        FrameLayout contentFramelayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflateResID, contentFramelayout);
        ButterKnife.bind(this);
        setToolbar();
        setUpNavigationView();
    }

    public void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(Html.fromHtml("<b>B I D Y A L A V</b>"));
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    public void setUpNavigationView() {
        Menu navMenu = navigationView.getMenu();
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.nav_profile:
                        goToMyProfileActivity();
                        break;
                    case R.id.nav_bidyalav_exam:
                        goToBidyalavExam();
                        break;
                    case R.id.nav_appeared_exam:
                        goToAppearedExamsActivity();
                        break;
                    case R.id.nav_bidyalav_appeared_exam:
                        goToBidyalavAppearedExamActivity();
                        break;
                    case R.id.nav_log_out:
                        goToLogin();
                        break;
                }
                return false;
            }
        });
    }

    private void goToMyProfileActivity() {
        Intent intent = new Intent(this, MyProfileActivity.class);
        startActivity(intent);
    }

    private void getUserinfo() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo != null) {
            Log.e("LogMsg", "Api Token: " + userInfo.apiToken);
            Log.e("LogMsg", "UserId: " + userInfo.id);
            Log.e("LogMsg", "User Type: " + userInfo.status);
        }
    }

    private void goToLogin() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setUserInfo(this, null);
        Intent intent = new Intent(this, LogInActivity.class);
        startActivity(intent);
        finish();
    }

    private void goToBidyalavExam() {
        Intent intent = new Intent(this, BidyalavExamActivity.class);
        startActivity(intent);
    }

    private void goToAppearedExamsActivity() {
        Intent intent = new Intent(this, AppearedExamActivity.class);
        startActivity(intent);
    }

    private void goToBidyalavAppearedExamActivity() {
        Intent intent = new Intent(this, BidyalavAppearedExamActivity.class);
        startActivity(intent);
    }

}