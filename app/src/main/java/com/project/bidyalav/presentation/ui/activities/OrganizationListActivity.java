package com.project.bidyalav.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.executors.impl.ThreadExecutor;
import com.project.bidyalav.presentation.presenters.OrganizationListPresenter;
import com.project.bidyalav.presentation.presenters.impl.OrganizationListPresenterImpl;
import com.project.bidyalav.presentation.ui.adapters.OrganizationAdapter;
import com.project.bidyalav.threading.MainThreadImpl;

public class OrganizationListActivity extends AppCompatActivity implements OrganizationListPresenter.View {

    @BindView(R.id.recycler_view_organizations)
    RecyclerView recyclerViewOrganizations;
    OrganizationListPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organization_list);
        getSupportActionBar().setTitle("All Organizations");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchOrganizationList();
    }

    private void initialisePresenter() {
        mPresenter = new OrganizationListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(OrganizationAdapter adapter) {
        recyclerViewOrganizations.setAdapter(adapter);
        recyclerViewOrganizations.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}