package com.project.bidyalav.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.executors.impl.ThreadExecutor;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.presentation.presenters.MyProfilePresenter;
import com.project.bidyalav.presentation.presenters.impl.MyProfilePresenterImpl;
import com.project.bidyalav.threading.MainThreadImpl;

import org.w3c.dom.Text;

import java.util.Calendar;

public class MyProfileActivity extends AppCompatActivity implements MyProfilePresenter.View {

    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_city)
    EditText editTextCity;
    @BindView(R.id.edit_text_fathers_name)
    EditText editTextFathersName;
    @BindView(R.id.edit_text_mobile)
    EditText editTextMobile;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    @BindView(R.id.txt_view_dob)
    TextView textViewDob;
    MyProfilePresenterImpl mPresenter;
    ProgressDialog progressDialog;
    String dob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("MY PROFILE");
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchUserProfile();
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new MyProfilePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }


    @Override
    public void loadUserProfile(UserInfo userInfo) {
        editTextName.setText(userInfo.name);
        editTextAddress.setText(userInfo.address);
        editTextCity.setText(userInfo.city);
        editTextEmail.setText(userInfo.email);
        editTextFathersName.setText(userInfo.fatherName);
        editTextMobile.setText(userInfo.mobile);
        editTextPin.setText(userInfo.pin);
        editTextState.setText(userInfo.state);
        textViewDob.setText(userInfo.dob);
        dob = userInfo.dob;
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.btn_update_profile) void onUpdateClicked() {
        if (    editTextName.getText().toString().trim().isEmpty() ||
                editTextState.getText().toString().trim().isEmpty() ||
                editTextPin.getText().toString().trim().isEmpty() ||
                editTextMobile.getText().toString().trim().isEmpty() ||
                editTextFathersName.getText().toString().trim().isEmpty() ||
                editTextCity.getText().toString().trim().isEmpty() ||
                editTextAddress.getText().toString().trim().isEmpty()
        ) {
            Toasty.warning(this, "All Fields Are Required").show();
        } else {
            mPresenter.updateProfile(
                    editTextName.getText().toString(),
                    editTextFathersName.getText().toString(),
                    editTextMobile.getText().toString(),
                    dob,
                    editTextAddress.getText().toString(),
                    editTextState.getText().toString(),
                    editTextCity.getText().toString(),
                    editTextPin.getText().toString()
            );
        }
    }

    @OnClick(R.id.dob_linear_layout) void onDOBLayoutClicked() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        textViewDob.setText(dob);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(Color.BLUE);
    }

}