package com.project.bidyalav.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.models.Answer;
import com.project.bidyalav.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AnswerAdapter extends RecyclerView.Adapter<AnswerAdapter.ViewHolder> {

    public interface Callback {
        void onAnswerSelected(int id);
    }

    Context mContext;
    Answer[] answers;
    Callback mCallback;

    public AnswerAdapter(Context mContext, Answer[] answers, Callback mCallback) {
        this.mContext = mContext;
        this.answers = answers;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_answers, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if  (answers[position].isSelected) {
            holder.imgViewQuestionCheckBox.setVisibility(View.VISIBLE);
        } else {
            holder.imgViewQuestionCheckBox.setVisibility(View.INVISIBLE);
        }
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onAnswerSelected(answers[position].id);
            }
        });
        if (answers[position].optionType == 1) {
            holder.txtViewQuestion.setText(answers[position].option);
            holder.imgViewAnswer.setVisibility(View.GONE);
            holder.txtViewQuestion.setVisibility(View.VISIBLE);
        } else {
            GlideHelper.setImageView(mContext, holder.imgViewAnswer, mContext.getResources().getString(R.string.base_url) + "api/view/exam/" + answers[position].option);
            holder.imgViewAnswer.setVisibility(View.VISIBLE);
            holder.txtViewQuestion.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return answers.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_question)
        TextView txtViewQuestion;
        @BindView(R.id.question_check_box)
        ImageView imgViewQuestionCheckBox;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.img_view_answer)
        ImageView imgViewAnswer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataset(Answer[] answers) {
        this.answers = answers;
        notifyDataSetChanged();
    }

}
