package com.project.bidyalav.presentation.presenters.impl;

import android.content.Context;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchAppearedExamInteractor;
import com.project.bidyalav.domain.interactors.impl.FetchAppearedExamInteractorImpl;
import com.project.bidyalav.domain.models.AppearedExam;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.presentation.presenters.AppearedExamPresenter;
import com.project.bidyalav.presentation.presenters.base.AbstractPresenter;
import com.project.bidyalav.presentation.ui.adapters.AppearedExamAdapter;
import com.project.bidyalav.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class AppearedExamPresenterImpl extends AbstractPresenter implements AppearedExamPresenter, FetchAppearedExamInteractor.Callback {

    Context mContext;
    AppearedExamPresenter.View mView;
    FetchAppearedExamInteractorImpl fetchAppearedExamInteractor;

    public AppearedExamPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchAppearedExams() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.error(mContext, "Session Expired !! Please Login Again").show();
        } else {
            fetchAppearedExamInteractor = new FetchAppearedExamInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id);
            fetchAppearedExamInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingAppearedExamSuccess(AppearedExam[] appearedExams) {
        AppearedExamAdapter adapter = new AppearedExamAdapter(mContext, appearedExams);
        mView.loadAppearedExamsAdapter(adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingAppearedExamFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
