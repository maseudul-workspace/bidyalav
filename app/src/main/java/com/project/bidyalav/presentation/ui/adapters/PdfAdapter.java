package com.project.bidyalav.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.R;
import com.project.bidyalav.domain.models.PDF;
import com.project.bidyalav.domain.models.UserInfo;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PdfAdapter extends RecyclerView.Adapter<PdfAdapter.ViewHolder> {

    public interface Callback {
        void onPdfClicked(String pdfUrl);
    }

    Context mContext;
    PDF[] pdfs;
    Callback mCallback;

    public PdfAdapter(Context mContext, PDF[] pdfs, Callback callback) {
        this.mContext = mContext;
        this.pdfs = pdfs;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_pdf, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewPdfName.setText(pdfs[position].name);
        holder.btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onPdfClicked(pdfs[position].pdfFile);
            }
        });
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo.status == 1) {
            if (pdfs[position].status == 1) {
               holder.layoutPremiumContent.setVisibility(View.GONE);
               holder.btnBuy.setVisibility(View.GONE);
               holder.btnView.setVisibility(View.VISIBLE);
            } else {
                holder.layoutPremiumContent.setVisibility(View.VISIBLE);
                holder.btnBuy.setVisibility(View.VISIBLE);
                holder.btnView.setVisibility(View.GONE);
            }
        } else {
            holder.layoutPremiumContent.setVisibility(View.GONE);
            holder.btnBuy.setVisibility(View.GONE);
            holder.btnView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return pdfs.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_pdf_name)
        TextView txtViewPdfName;
        @BindView(R.id.btn_view)
        Button btnView;
        @BindView(R.id.layout_premium_content)
        View layoutPremiumContent;
        @BindView(R.id.btn_buy)
        Button btnBuy;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
