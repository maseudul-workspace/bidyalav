package com.project.bidyalav.presentation.presenters;

import com.project.bidyalav.presentation.ui.adapters.ExamsAdapter;
import com.project.bidyalav.presentation.ui.adapters.PdfAdapter;
import com.project.bidyalav.presentation.ui.adapters.VideoListAdapter;

public interface SubjectDetailsPresenter {
    void fetchExamsList(int subjectId);
    void fetchPdfList(int subjectId);
    void fetchVideoList(int subjectId);
    interface View {
        void loadExams(ExamsAdapter examsAdapter);
        void loadPdfs(PdfAdapter pdfAdapter);
        void loadVideoList(VideoListAdapter videoListAdapter);
        void goToViewPdfActivity(String pdf);
        void goToExamDetailsActivity(int examId);
        void goToVideoPreviewActivity(String videoId);
        void showLoader();
        void hideLoader();
    }
}
