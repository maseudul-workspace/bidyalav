package com.project.bidyalav.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.models.Subject;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SubjectMainAdapter extends RecyclerView.Adapter<SubjectMainAdapter.ViewHolder> {

    public interface Callback {
        void onSubjectClicked(int id);
    }

    Context mContext;
    Subject[] subjects;
    Callback mCallback;

    public SubjectMainAdapter(Context mContext, Subject[] subjects, Callback mCallback) {
        this.mContext = mContext;
        this.subjects = subjects;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_subjects_main, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSubject.setText(subjects[position].name);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSubjectClicked(subjects[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subjects.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_subject)
        TextView txtViewSubject;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
