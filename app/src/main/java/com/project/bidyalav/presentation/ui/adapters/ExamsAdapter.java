package com.project.bidyalav.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.R;
import com.project.bidyalav.domain.models.Exam;
import com.project.bidyalav.domain.models.UserInfo;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ExamsAdapter extends RecyclerView.Adapter<ExamsAdapter.ViewHolder> {

    public interface Callback {
        void onExamClicked(int id);
    }

    Context mContext;
    Exam[] exams;
    Callback mCallback;

    public ExamsAdapter(Context mContext, Exam[] exams, Callback mCallback) {
        this.mContext = mContext;
        this.exams = exams;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_exams, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewExamName.setText(exams[position].examName);
        holder.btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onExamClicked(exams[position].id);
            }
        });
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo.status == 1) {
            if (exams[position].examType == 1) {
                if (exams[position].examStatus == 1) {
                    holder.layoutExamNotStarted.setVisibility(View.VISIBLE);
                    holder.layoutExamStarted.setVisibility(View.GONE);
                    holder.layoutExamFinished.setVisibility(View.GONE);
                    holder.btnStart.setVisibility(View.GONE);
                } else if (exams[position].examStatus == 2) {
                    holder.layoutExamNotStarted.setVisibility(View.GONE);
                    holder.layoutExamStarted.setVisibility(View.VISIBLE);
                    holder.layoutExamFinished.setVisibility(View.GONE);
                    holder.btnStart.setVisibility(View.VISIBLE);
                } else {
                    holder.layoutExamNotStarted.setVisibility(View.GONE);
                    holder.layoutExamStarted.setVisibility(View.GONE);
                    holder.layoutExamFinished.setVisibility(View.VISIBLE);
                    holder.btnStart.setVisibility(View.GONE);
                }
                holder.layoutPremiumContent.setVisibility(View.GONE);
                holder.btnBuy.setVisibility(View.GONE);
            } else {
                holder.layoutPremiumContent.setVisibility(View.VISIBLE);
                holder.btnBuy.setVisibility(View.VISIBLE);
                holder.layoutExamNotStarted.setVisibility(View.GONE);
                holder.layoutExamStarted.setVisibility(View.GONE);
                holder.layoutExamFinished.setVisibility(View.GONE);
                holder.btnStart.setVisibility(View.GONE);
            }
        } else {
            if (exams[position].examStatus == 1) {
                holder.layoutExamNotStarted.setVisibility(View.VISIBLE);
                holder.layoutExamStarted.setVisibility(View.GONE);
                holder.layoutExamFinished.setVisibility(View.GONE);
                holder.btnStart.setVisibility(View.GONE);
            } else if (exams[position].examStatus == 2) {
                holder.layoutExamNotStarted.setVisibility(View.GONE);
                holder.layoutExamStarted.setVisibility(View.VISIBLE);
                holder.layoutExamFinished.setVisibility(View.GONE);
                holder.btnStart.setVisibility(View.VISIBLE);
            } else {
                holder.layoutExamNotStarted.setVisibility(View.GONE);
                holder.layoutExamStarted.setVisibility(View.GONE);
                holder.layoutExamFinished.setVisibility(View.VISIBLE);
                holder.btnStart.setVisibility(View.GONE);
            }
            holder.layoutPremiumContent.setVisibility(View.GONE);
            holder.btnBuy.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return exams.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_exam_name)
        TextView txtViewExamName;
        @BindView(R.id.btn_start)
        Button btnStart;
        @BindView(R.id.layout_exam_finished)
        View layoutExamFinished;
        @BindView(R.id.layout_exam_started)
        View layoutExamStarted;
        @BindView(R.id.layout_exam_not_started)
        View layoutExamNotStarted;
        @BindView(R.id.layout_premium_content)
        View layoutPremiumContent;
        @BindView(R.id.btn_buy)
        Button btnBuy;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
