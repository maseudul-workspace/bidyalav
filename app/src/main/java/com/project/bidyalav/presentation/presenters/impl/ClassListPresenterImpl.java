package com.project.bidyalav.presentation.presenters.impl;

import android.content.Context;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchClassListInteractor;
import com.project.bidyalav.domain.interactors.impl.FetchClassListInteractorImpl;
import com.project.bidyalav.domain.models.Class;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.presentation.presenters.ClassListPresenter;
import com.project.bidyalav.presentation.presenters.base.AbstractPresenter;
import com.project.bidyalav.presentation.ui.adapters.ClassMainAdapter;
import com.project.bidyalav.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ClassListPresenterImpl extends AbstractPresenter implements    ClassListPresenter,
                                                                            FetchClassListInteractor.Callback,
                                                                            ClassMainAdapter.Callback
{

    Context mContext;
    ClassListPresenter.View mView;
    FetchClassListInteractorImpl fetchClassListInteractor;

    public ClassListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchClassList() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.error(mContext, "Session Expired !! Please Login Again").show();
        } else {
            fetchClassListInteractor = new FetchClassListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.orgId);
            fetchClassListInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingClassListSuccess(Class[] classes) {
        ClassMainAdapter adapter = new ClassMainAdapter(mContext, classes, this);
        mView.hideLoader();
        mView.loadAdapter(adapter);
    }

    @Override
    public void onGettingClassListFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onSubjectClicked(int subjectId) {
        mView.goToSubjectDetails(subjectId);
    }
}
