package com.project.bidyalav.presentation.ui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.executors.impl.ThreadExecutor;
import com.project.bidyalav.domain.models.VideoList;
import com.project.bidyalav.presentation.presenters.SubjectDetailsPresenter;
import com.project.bidyalav.presentation.presenters.impl.SubjectDetailsPresenterImpl;
import com.project.bidyalav.presentation.ui.activities.VideoPreviewActivity;
import com.project.bidyalav.presentation.ui.adapters.ExamsAdapter;
import com.project.bidyalav.presentation.ui.adapters.PdfAdapter;
import com.project.bidyalav.presentation.ui.adapters.VideoListAdapter;
import com.project.bidyalav.threading.MainThreadImpl;

import java.util.ArrayList;


public class VideosFragment extends Fragment implements SubjectDetailsPresenter.View {

    @BindView(R.id.recycler_view_videos)
    RecyclerView recyclerViewVideos;
    Context mContext;
    int subjectId;
    SubjectDetailsPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    public VideosFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static VideosFragment newInstance() {
        VideosFragment fragment = new VideosFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_videos, container, false);
        ButterKnife.bind(this, view);
        subjectId = getArguments().getInt("subjectId");
        mPresenter.fetchVideoList(subjectId);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        setUpProgressDialog();
        initialisePresenter();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initialisePresenter() {
        mPresenter = new SubjectDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(),mContext, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadExams(ExamsAdapter examsAdapter) {

    }

    @Override
    public void loadPdfs(PdfAdapter pdfAdapter) {

    }

    @Override
    public void loadVideoList(VideoListAdapter videoListAdapter) {
        recyclerViewVideos.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewVideos.setAdapter(videoListAdapter);
    }

    @Override
    public void goToViewPdfActivity(String pdf) {

    }

    @Override
    public void goToExamDetailsActivity(int examId) {

    }

    @Override
    public void goToVideoPreviewActivity(String videoId) {
        Intent intent = new Intent(mContext, VideoPreviewActivity.class);
        intent.putExtra("VIDEO_ID", videoId);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
    }

    @Override
    public void hideLoader() {

    }
}