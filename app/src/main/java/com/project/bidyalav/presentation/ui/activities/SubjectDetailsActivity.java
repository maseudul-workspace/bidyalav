package com.project.bidyalav.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.project.bidyalav.R;
import com.project.bidyalav.presentation.ui.adapters.ViewPagerFragmentAdapter;
import com.project.bidyalav.presentation.ui.fragments.ExamFragment;
import com.project.bidyalav.presentation.ui.fragments.PdfFragment;
import com.project.bidyalav.presentation.ui.fragments.VideosFragment;

public class SubjectDetailsActivity extends AppCompatActivity {

    @BindView(R.id.tablayout_id)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    ViewPagerFragmentAdapter viewPagerAdapter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    int subjectId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Subject Details");
        subjectId = getIntent().getIntExtra("subjectId", 0);
        setTabLayout();
    }

    private void setTabLayout() {
        Bundle bundle = new Bundle();
        bundle.putInt("subjectId", subjectId);
        ExamFragment examFragment = new ExamFragment();
        examFragment.setArguments(bundle);
        PdfFragment pdfFragment = new PdfFragment();
        pdfFragment.setArguments(bundle);
        VideosFragment videosFragment = new VideosFragment();
        videosFragment.setArguments(bundle);
        viewPagerAdapter = new ViewPagerFragmentAdapter(getSupportFragmentManager());
        viewPagerAdapter.AddFragment(examFragment, "Exams");
        viewPagerAdapter.AddFragment(pdfFragment, "Pdf");
        viewPagerAdapter.AddFragment(videosFragment, "Videos");
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(viewPager);
    }

}