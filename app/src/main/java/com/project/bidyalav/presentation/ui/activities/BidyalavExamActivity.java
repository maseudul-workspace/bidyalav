package com.project.bidyalav.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.executors.impl.ThreadExecutor;
import com.project.bidyalav.presentation.presenters.BidyalavExamPresenter;
import com.project.bidyalav.presentation.presenters.impl.BidyalavExamPresenterImpl;
import com.project.bidyalav.presentation.ui.adapters.BidyalavExamsAdapter;
import com.project.bidyalav.presentation.ui.adapters.ExamsAdapter;
import com.project.bidyalav.threading.MainThreadImpl;

public class BidyalavExamActivity extends AppCompatActivity implements BidyalavExamPresenter.View {

    @BindView(R.id.recycler_view_exams)
    RecyclerView recyclerViewExams;
    BidyalavExamPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bidyalav_exam);
        getSupportActionBar().setTitle("Bidyalav Exam");
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchBidyalavExam();
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new BidyalavExamPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(BidyalavExamsAdapter adapter) {
        recyclerViewExams.setAdapter(adapter);
        recyclerViewExams.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void goToExamDetailsActivity(int examId) {
        Intent intent = new Intent(this, BidyalavExamDetailsActivity.class);
        intent.putExtra("examId", examId);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}