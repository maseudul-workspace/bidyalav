package com.project.bidyalav.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.R;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.domain.models.VideoList;
import com.project.bidyalav.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder> {

    public interface Callback {
        void onViewClicked(String videoId);
    }

    Context mContext;
    VideoList[] videoLists;
    Callback mCallback;

    public VideoListAdapter(Context mContext, VideoList[] videoLists, Callback mCallback) {
        this.mContext = mContext;
        this.videoLists = videoLists;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_video_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        holder.txtViewVideoHeading.setText(videoLists.get(position).videoName);
//        holder.txtViewDesc.setText(videoLists.get(position).videoDesc);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewVideoThumbnail, "https://img.youtube.com/vi/" + videoLists[position].videoId + "/0.jpg", 10);
        holder.btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(videoLists[position].videoId);
            }
        });
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo.status == 1) {
            if (videoLists[position].status == 1) {
                holder.layoutTransparent.setVisibility(View.VISIBLE);
                holder.imageViewLock.setVisibility(View.VISIBLE);
                holder.btnBuy.setVisibility(View.VISIBLE);
                holder.btnView.setVisibility(View.GONE);
            } else {
                holder.layoutTransparent.setVisibility(View.GONE);
                holder.imageViewLock.setVisibility(View.GONE);
                holder.btnBuy.setVisibility(View.GONE);
                holder.btnView.setVisibility(View.VISIBLE);
            }
        } else {
            holder.layoutTransparent.setVisibility(View.GONE);
            holder.imageViewLock.setVisibility(View.GONE);
            holder.btnBuy.setVisibility(View.GONE);
            holder.btnView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return videoLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_video_thumbnail)
        ImageView imgViewVideoThumbnail;
        @BindView(R.id.txt_view_video_heading)
        TextView txtViewVideoHeading;
        @BindView(R.id.txt_video_desc)
        TextView txtViewDesc;
        @BindView(R.id.btn_view)
        Button btnView;
        @BindView(R.id.layout_transparent)
        View layoutTransparent;
        @BindView(R.id.img_view_lock)
        ImageView imageViewLock;
        @BindView(R.id.btn_buy)
        Button btnBuy;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
