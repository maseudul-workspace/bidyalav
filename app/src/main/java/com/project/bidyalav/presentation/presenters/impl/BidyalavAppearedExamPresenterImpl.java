package com.project.bidyalav.presentation.presenters.impl;

import android.content.Context;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchBidyalavAppearedExamInteractor;
import com.project.bidyalav.domain.interactors.impl.FetchBidyalavAppearedExamInteractorImpl;
import com.project.bidyalav.domain.models.AppearedExam;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.presentation.presenters.BidyalavAppearedExamPresenter;
import com.project.bidyalav.presentation.presenters.base.AbstractPresenter;
import com.project.bidyalav.presentation.ui.adapters.AppearedExamAdapter;
import com.project.bidyalav.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class BidyalavAppearedExamPresenterImpl extends AbstractPresenter implements BidyalavAppearedExamPresenter, FetchBidyalavAppearedExamInteractor.Callback {

    Context mContext;
    BidyalavAppearedExamPresenter.View mView;
    FetchBidyalavAppearedExamInteractorImpl fetchBidyalavAppearedExamInteractor;

    public BidyalavAppearedExamPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchBidyalavAppearedExams() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.error(mContext, "Session Expired !! Please Login Again").show();
        } else {
            fetchBidyalavAppearedExamInteractor = new FetchBidyalavAppearedExamInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id);
            fetchBidyalavAppearedExamInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingBidyalavAppearedExamSuccess(AppearedExam[] appearedExams) {
        AppearedExamAdapter adapter = new AppearedExamAdapter(mContext, appearedExams);
        mView.loadBidyalavAppearedExamsAdapter(adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingBidayalavAppearedExamFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
