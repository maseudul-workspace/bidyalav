package com.project.bidyalav.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.CheckLoginInteractor;
import com.project.bidyalav.domain.interactors.impl.CheckLoginInteractorImpl;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.presentation.presenters.LoginPresenter;
import com.project.bidyalav.presentation.presenters.base.AbstractPresenter;
import com.project.bidyalav.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter, CheckLoginInteractor.Callback {

    Context mContext;
    LoginPresenter.View mView;
    CheckLoginInteractorImpl checkLoginInteractor;
    AndroidApplication androidApplication;

    public LoginPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkLogin(String email, String password) {
        checkLoginInteractor = new CheckLoginInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, email, password);
        checkLoginInteractor.execute();
    }

    @Override
    public void onCheckLoginSuccess(UserInfo userInfo) {
        mView.hideLoader();
        Toasty.success(mContext, "Login Successful", Toast.LENGTH_SHORT).show();
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        mView.goToMainActivity();
    }

    @Override
    public void onCheckLoginFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg).show();
    }
}
