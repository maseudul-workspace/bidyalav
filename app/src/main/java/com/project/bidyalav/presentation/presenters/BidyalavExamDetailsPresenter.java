package com.project.bidyalav.presentation.presenters;

import com.project.bidyalav.domain.models.ExamDetails;

public interface BidyalavExamDetailsPresenter {
    void startExam(int examId);
    void submitAnswer(int studentExamId, int questionId, int answerId);
    void endExam(int studentExamId, int questionId, int answerId);
    interface View {
        void loadExamDetails(ExamDetails examDetails);
        void onAnswerSubmittedSuccess();
        void onEndExamSuccess();
        void showLoader();
        void hideLoader();
    }
}
