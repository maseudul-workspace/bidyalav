package com.project.bidyalav.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.executors.impl.ThreadExecutor;
import com.project.bidyalav.presentation.presenters.MainPresenter;
import com.project.bidyalav.presentation.presenters.impl.MainPresenterImpl;
import com.project.bidyalav.presentation.ui.adapters.ClassMainAdapter;
import com.project.bidyalav.presentation.ui.adapters.OrganizationHorizontalAdapter;
import com.project.bidyalav.threading.MainThreadImpl;
import com.project.bidyalav.util.GlideHelper;

import java.util.ArrayList;

public class MainActivity extends BaseActivity implements MainPresenter.View {

    MainPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        ButterKnife.bind(this);
        setUpProgressDialog();
        initialisePresenter();
//        mPresenter.fetchMainData();
    }

    private void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadData(String organizationImage, ClassMainAdapter classMainAdapter, OrganizationHorizontalAdapter organizationHorizontalAdapter) {

    }

    @Override
    public void goToSubjectDetails(int subjectId) {
        Intent intent = new Intent(this, SubjectDetailsActivity.class);
        intent.putExtra("subjectId", subjectId);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.card_view_class) void onClassCardViewClicked() {
        Intent intent = new Intent(this, ClassListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.card_view_organization) void onOrganizationClicked() {
        Intent intent = new Intent(this, OrganizationListActivity.class);
        startActivity(intent);
    }


}