package com.project.bidyalav.presentation.presenters.impl;

import android.content.Context;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchBidyalavExamListInteractor;
import com.project.bidyalav.domain.interactors.impl.FetchBidyalavExamListInteractorImpl;
import com.project.bidyalav.domain.models.BidyalavExamList;
import com.project.bidyalav.domain.models.Exam;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.presentation.presenters.BidyalavExamPresenter;
import com.project.bidyalav.presentation.presenters.base.AbstractPresenter;
import com.project.bidyalav.presentation.ui.adapters.BidyalavExamsAdapter;
import com.project.bidyalav.presentation.ui.adapters.ExamsAdapter;
import com.project.bidyalav.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class BidyalavExamPresenterImpl extends AbstractPresenter implements BidyalavExamPresenter,
                                                                            FetchBidyalavExamListInteractor.Callback,
                                                                            BidyalavExamsAdapter.Callback
{

    Context mContext;
    BidyalavExamPresenter.View mView;
    FetchBidyalavExamListInteractorImpl fetchBidyalavExamListInteractor;

    public BidyalavExamPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchBidyalavExam() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.error(mContext, "Session Expired !! Please Login Again").show();
        } else {
            fetchBidyalavExamListInteractor = new FetchBidyalavExamListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id);
            fetchBidyalavExamListInteractor.execute();
        }
    }

    @Override
    public void onGettingBidyalavExamSuccess(BidyalavExamList[] exams) {
        BidyalavExamsAdapter adapter = new BidyalavExamsAdapter(mContext, exams, this);
        mView.loadAdapter(adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingBidyalavExamFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onExamClicked(int id) {
        mView.goToExamDetailsActivity(id);
    }
}
