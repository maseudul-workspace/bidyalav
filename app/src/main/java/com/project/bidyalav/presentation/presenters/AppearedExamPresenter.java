package com.project.bidyalav.presentation.presenters;

import com.project.bidyalav.presentation.ui.adapters.AppearedExamAdapter;

public interface AppearedExamPresenter {
    void fetchAppearedExams();
    interface View {
        void loadAppearedExamsAdapter(AppearedExamAdapter adapter);
        void showLoader();
        void hideLoader();
    }
}
