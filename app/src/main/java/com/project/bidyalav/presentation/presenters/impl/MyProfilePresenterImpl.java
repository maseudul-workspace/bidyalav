package com.project.bidyalav.presentation.presenters.impl;

import android.content.Context;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchUserProfileInteractor;
import com.project.bidyalav.domain.interactors.UpdateProfileInteractor;
import com.project.bidyalav.domain.interactors.impl.FetchUserProfileInteractorImpl;
import com.project.bidyalav.domain.interactors.impl.UpdateProfileInteractorImpl;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.presentation.presenters.MyProfilePresenter;
import com.project.bidyalav.presentation.presenters.base.AbstractPresenter;
import com.project.bidyalav.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class MyProfilePresenterImpl extends AbstractPresenter implements    MyProfilePresenter,
                                                                            FetchUserProfileInteractor.Callback,
                                                                            UpdateProfileInteractor.Callback
{

    Context mContext;
    MyProfilePresenter.View mView;
    FetchUserProfileInteractorImpl fetchUserProfileInteractor;
    UpdateProfileInteractorImpl updateProfileInteractor;

    public MyProfilePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchUserProfile() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.error(mContext, "Session Expired !! Please Login Again").show();
        } else {
            fetchUserProfileInteractor = new FetchUserProfileInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id);
            fetchUserProfileInteractor.execute();
        }
    }

    @Override
    public void updateProfile(String name, String fathersName, String mobile, String dob, String address, String state, String city, String pin) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.error(mContext, "Session Expired !! Please Login Again").show();
        } else {
            updateProfileInteractor = new UpdateProfileInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id, name, fathersName, mobile, dob, address, state, city, pin);
            updateProfileInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingUserProfileSuccess(UserInfo userInfo) {
        mView.loadUserProfile(userInfo);
        mView.hideLoader();
    }

    @Override
    public void onGettingUserProfileFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg);
    }

    @Override
    public void onUserUpdateSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Updated Successfully").show();
    }

    @Override
    public void onUserUpdateFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
