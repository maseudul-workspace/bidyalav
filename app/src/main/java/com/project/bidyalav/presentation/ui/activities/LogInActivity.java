package com.project.bidyalav.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.android.material.textfield.TextInputLayout;
import com.project.bidyalav.R;
import com.project.bidyalav.domain.executors.impl.ThreadExecutor;
import com.project.bidyalav.presentation.presenters.LoginPresenter;
import com.project.bidyalav.presentation.presenters.impl.LoginPresenterImpl;
import com.project.bidyalav.threading.MainThreadImpl;

public class LogInActivity extends AppCompatActivity implements LoginPresenter.View {

    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.txt_input_email_layout)
    TextInputLayout textInputEmailLayout;
    @BindView(R.id.txt_input_password_layout)
    TextInputLayout textInputPasswordLayout;
    ProgressDialog progressDialog;
    LoginPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("LOG IN");
        setUpProgressDialog();
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.btn_log_in) void onLoginClicked() {
        textInputEmailLayout.setError("");
        textInputPasswordLayout.setError("");
        if (editTextEmail.getText().toString().trim().isEmpty()) {
            textInputEmailLayout.setError("Please Enter Email");
        } else if (editTextPassword.getText().toString().trim().isEmpty()) {
            textInputPasswordLayout.setError("Please Enter Password");
        } else {
            mPresenter.checkLogin(editTextEmail.getText().toString(), editTextPassword.getText().toString());
            showLoader();
        }
    }

    @OnClick(R.id.register_layout) void onRegisterLayout() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}