package com.project.bidyalav.presentation.presenters;

import com.project.bidyalav.presentation.ui.adapters.BidyalavExamsAdapter;
import com.project.bidyalav.presentation.ui.adapters.ExamsAdapter;

public interface BidyalavExamPresenter {
    void fetchBidyalavExam();
    interface View {
        void loadAdapter(BidyalavExamsAdapter adapter);
        void goToExamDetailsActivity(int examId);
        void showLoader();
        void hideLoader();
    }
}
