package com.project.bidyalav.presentation.presenters.impl;

import android.content.Context;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchOrganizationiListInteractor;
import com.project.bidyalav.domain.interactors.impl.FetchOrganizationListInteractorImpl;
import com.project.bidyalav.domain.models.Organization;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.presentation.presenters.OrganizationListPresenter;
import com.project.bidyalav.presentation.presenters.base.AbstractPresenter;
import com.project.bidyalav.presentation.ui.adapters.OrganizationAdapter;
import com.project.bidyalav.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class OrganizationListPresenterImpl extends AbstractPresenter implements OrganizationListPresenter,
                                                                                FetchOrganizationiListInteractor.Callback,
                                                                                OrganizationAdapter.Callback
{

    Context mContext;
    OrganizationListPresenter.View mView;
    FetchOrganizationListInteractorImpl fetchOrganizationListInteractor;

    public OrganizationListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchOrganizationList() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.error(mContext, "Session Expired !! Please Login Again").show();
        } else {
            fetchOrganizationListInteractor = new FetchOrganizationListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id);
            fetchOrganizationListInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingOrganizationListSuccess(Organization[] organizations) {
        OrganizationAdapter adapter = new OrganizationAdapter(mContext, organizations, this);
        mView.loadAdapter(adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingOrganizationListFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
