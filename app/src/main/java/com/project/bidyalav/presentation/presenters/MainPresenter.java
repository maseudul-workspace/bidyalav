package com.project.bidyalav.presentation.presenters;

import com.project.bidyalav.presentation.ui.adapters.ClassMainAdapter;
import com.project.bidyalav.presentation.ui.adapters.OrganizationHorizontalAdapter;

public interface MainPresenter {
    void fetchMainData();
    interface View {
        void loadData(String organizationImage, ClassMainAdapter classMainAdapter, OrganizationHorizontalAdapter organizationHorizontalAdapter);
        void goToSubjectDetails(int subjectId);
        void showLoader();
        void hideLoader();
    }
}
