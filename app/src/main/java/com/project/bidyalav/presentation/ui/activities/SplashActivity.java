package com.project.bidyalav.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.R;
import com.project.bidyalav.domain.models.UserInfo;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isLoggedIn()) {
                    goToMainActivity();
                } else {
                    goToSliderSplashActivity();
                }
            }
        }, 3000);
    }

    private boolean isLoggedIn() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo == null) {
            return false;
        } else {
            return true;
        }
    }

    private void goToLogin() {
        Intent intent = new Intent(this, LogInActivity.class);
        startActivity(intent);
    }

    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void goToSliderSplashActivity() {
        Intent intent = new Intent(this, SliderSplashActivity.class);
        startActivity(intent);
    }

}