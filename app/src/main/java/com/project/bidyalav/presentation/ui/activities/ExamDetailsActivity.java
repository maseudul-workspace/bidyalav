package com.project.bidyalav.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.executors.impl.ThreadExecutor;
import com.project.bidyalav.domain.models.Answer;
import com.project.bidyalav.domain.models.AppearedQuestion;
import com.project.bidyalav.domain.models.Exam;
import com.project.bidyalav.domain.models.ExamDetails;
import com.project.bidyalav.domain.models.Question;
import com.project.bidyalav.presentation.presenters.ExamDetailsPresenter;
import com.project.bidyalav.presentation.presenters.impl.ExamDetailsPresenterImpl;
import com.project.bidyalav.presentation.ui.adapters.AnswerAdapter;
import com.project.bidyalav.threading.MainThreadImpl;
import com.project.bidyalav.util.GlideHelper;

public class ExamDetailsActivity extends AppCompatActivity implements ExamDetailsPresenter.View, AnswerAdapter.Callback {

    @BindView(R.id.recycler_view_question)
    RecyclerView recyclerViewQuestion;
    @BindView(R.id.txt_view_question)
    TextView txtViewQuestion;
    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.btn_prev)
    Button btnPrev;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.question_image_layout)
    View questionImageLayout;
    @BindView(R.id.img_view_question)
    ImageView imageViewQuestion;
    ProgressDialog progressDialog;
    ExamDetailsPresenterImpl mPresenter;
    int questionNo = 0;
    Question[] questions;
    int examId;
    Answer[] answers;
    AppearedQuestion[] appearedQuestions;
    AnswerAdapter answerAdapter;
    int selectedAnswerId = 0;
    ExamDetails examDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_details);
        examId = getIntent().getIntExtra("examId", 0);
        ButterKnife.bind(this);
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.startExam(examId);
    }

    private void initialisePresenter() {
        mPresenter = new ExamDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadExamDetails(ExamDetails examDetails) {
        this.examDetails = examDetails;
        this.appearedQuestions = examDetails.appearedQuestions;
        questions = examDetails.questions;
        answers = questions[questionNo].answers;
        if (checkIsQuestionAppeared(questions[questionNo].id)) {
            for (int i = 0; i < answers.length; i++) {
                if (checkIsAnswerSelected(answers[i].id)) {
                    answers[i].isSelected = true;
                    selectedAnswerId = answers[i].id;
                } else {
                    answers[i].isSelected = false;
                }
            }
        }
        if (questions[questionNo].questionType == 1) {
            txtViewQuestion.setText("1. " + questions[questionNo].question);
            questionImageLayout.setVisibility(View.GONE);
            txtViewQuestion.setVisibility(View.VISIBLE);
        } else {
            GlideHelper.setImageView(this, imageViewQuestion, getResources().getString(R.string.base_url) + "api/view/exam/" + questions[questionNo].question);
            questionImageLayout.setVisibility(View.VISIBLE);
            txtViewQuestion.setVisibility(View.GONE);
        }
        answerAdapter = new AnswerAdapter(this, questions[questionNo].answers, this);
        recyclerViewQuestion.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewQuestion.setAdapter(answerAdapter);
    }

    @Override
    public void onAnswerSubmittedSuccess() {
        selectedAnswerId = 0;
        btnPrev.setVisibility(View.VISIBLE);
        questionNo++;
        answers = questions[questionNo].answers;
        if (checkIsQuestionAppeared(questions[questionNo].id)) {
            for (int i = 0; i < answers.length; i++) {
                if (checkIsAnswerSelected(answers[i].id)) {
                    answers[i].isSelected = true;
                    selectedAnswerId = answers[i].id;
                } else {
                    answers[i].isSelected = false;
                }
            }
        }
        if (questions[questionNo].questionType == 1) {
            txtViewQuestion.setText(questionNo + 1 + ". " + questions[questionNo].question);
            questionImageLayout.setVisibility(View.GONE);
            txtViewQuestion.setVisibility(View.VISIBLE);
        } else {
            GlideHelper.setImageView(this, imageViewQuestion, getResources().getString(R.string.base_url) + "api/view/exam/" + questions[questionNo].question);
            questionImageLayout.setVisibility(View.VISIBLE);
            txtViewQuestion.setVisibility(View.GONE);
        }
        answerAdapter = new AnswerAdapter(this, questions[questionNo].answers, this);
        recyclerViewQuestion.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewQuestion.setAdapter(answerAdapter);
        if(questionNo == questions.length - 1) {
            btnSubmit.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.GONE);
        }
    }

    @Override
    public void onEndExamSuccess() {
        Toasty.success(this, "Exam Completed Successfully").show();
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onAnswerSelected(int id) {
        this.selectedAnswerId = id;
        for (int i = 0; i < answers.length; i++) {
            if (answers[i].id == id) {
                answers[i].isSelected = true;
            } else {
                answers[i].isSelected = false;
            }
        }
        answerAdapter.updateDataset(answers);
    }

    @OnClick(R.id.btn_next) void onNextClicked() {
        if (selectedAnswerId == 0) {
            Toasty.warning(this, "Please Select An Option").show();
        } else {
            mPresenter.submitAnswer(this.examDetails.studentExamId, questions[questionNo].id, selectedAnswerId);
        }
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (selectedAnswerId == 0) {
            Toasty.warning(this, "Please Select An Option").show();
        } else {
            mPresenter.endExam(this.examDetails.studentExamId, questions[questionNo].id, selectedAnswerId);
        }
    }

    @OnClick(R.id.btn_prev) void onPrevClicked() {
        btnPrev.setVisibility(View.VISIBLE);
        questionNo--;
        answers = questions[questionNo].answers;
        for (int i = 0; i < answers.length; i++) {
            if (answers[i].isSelected) {
                selectedAnswerId = answers[i].id;
                break;
            }
        }
        if (questions[questionNo].questionType == 1) {
            txtViewQuestion.setText(questionNo + 1 + ". " + questions[questionNo].question);
            questionImageLayout.setVisibility(View.GONE);
            txtViewQuestion.setVisibility(View.VISIBLE);
        } else {
            GlideHelper.setImageView(this, imageViewQuestion, getResources().getString(R.string.base_url) + "api/view/exam/" + questions[questionNo].question);
            questionImageLayout.setVisibility(View.VISIBLE);
            txtViewQuestion.setVisibility(View.GONE);
        }
        answerAdapter = new AnswerAdapter(this, questions[questionNo].answers, this);
        recyclerViewQuestion.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewQuestion.setAdapter(answerAdapter);
        if(questionNo == questions.length - 1) {
            btnSubmit.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.GONE);
        }
        if (questionNo == 0) {
            btnPrev.setVisibility(View.GONE);
        }
    }

    private boolean checkIsQuestionAppeared(int questionId) {
        int flag = 0;
        for (int i = 0; i < appearedQuestions.length; i++) {
            if (appearedQuestions[i].questionId == questionId) {
                flag = 1;
            }
        }
        if (flag == 1) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkIsAnswerSelected(int answerId) {
        int flag = 0;
        for (int i = 0; i < appearedQuestions.length; i++) {
            if (appearedQuestions[i].answerId == answerId) {
                flag = 1;
            }
        }
        if (flag == 1) {
            return true;
        } else {
            return false;
        }
    }

}