package com.project.bidyalav.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.models.ImageTesting;
import com.project.bidyalav.presentation.ui.adapters.SplashSlidingAdapter;
import com.project.bidyalav.util.GlideHelper;

import java.util.ArrayList;

public class SliderSplashActivity extends AppCompatActivity {

    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.dots_indicator_layout)
    LinearLayout dotsIndicatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider_splash);
        ButterKnife.bind(this);
        setViewPager();
    }

    private void setViewPager() {
        ArrayList<ImageTesting> images = new ArrayList<>();
        ImageTesting image1 = new ImageTesting("https://i.pinimg.com/originals/5d/16/a1/5d16a1340d38eec4dafa09d7ae3240dc.jpg");
        ImageTesting image2 = new ImageTesting("https://i.pinimg.com/originals/bf/9c/c5/bf9cc51125df3a4ebb569803ed324785.jpg");
        ImageTesting image3 = new ImageTesting("https://i.pinimg.com/originals/08/c5/50/08c5504b982d396c0b6b6bf3d367cbc3.jpg");
        images.add(image1);
        images.add(image2);
        images.add(image3);
        SplashSlidingAdapter adapter = new SplashSlidingAdapter(this, images);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareDotsIndicator(position, images.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        prepareDotsIndicator(0, images.size());
    }

    private void prepareDotsIndicator(int sliderPosition, int imageCount){
        if(dotsIndicatorLayout.getChildCount() > 0){
            dotsIndicatorLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[imageCount];
        for(int i = 0; i < imageCount; i++) {
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(18, 18);
            layoutParams.setMargins(5, 0, 5, 0);
            dotsIndicatorLayout.addView(dots[i], layoutParams);
        }
    }

    @OnClick(R.id.layout_sign_up) void onSignUpClicked() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_log_in) void onLogInClicked() {
        Intent intent = new Intent(this, LogInActivity.class);
        startActivity(intent);
    }

}