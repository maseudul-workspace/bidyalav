package com.project.bidyalav.presentation.presenters.impl;

import android.content.Context;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.EndBidyalavExamInteractor;
import com.project.bidyalav.domain.interactors.StartBidyalavExamInteractor;
import com.project.bidyalav.domain.interactors.StartExamInteractor;
import com.project.bidyalav.domain.interactors.SubmitAnswerInteractor;
import com.project.bidyalav.domain.interactors.SubmitBidyalavAnswerInteractor;
import com.project.bidyalav.domain.interactors.impl.EndBidyalavExamInteractorImpl;
import com.project.bidyalav.domain.interactors.impl.EndExamInteractorImpl;
import com.project.bidyalav.domain.interactors.impl.StartBidyalavExamInteratorImpl;
import com.project.bidyalav.domain.interactors.impl.StartExamInteractorImpl;
import com.project.bidyalav.domain.interactors.impl.SubmitAnswerInteractorImpl;
import com.project.bidyalav.domain.interactors.impl.SubmitBidyalavAnswerInteractorImpl;
import com.project.bidyalav.domain.models.ExamDetails;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.presentation.presenters.BidyalavExamDetailsPresenter;
import com.project.bidyalav.presentation.presenters.base.AbstractPresenter;
import com.project.bidyalav.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class BidyalavExamDetailsPresenterImpl extends AbstractPresenter implements  BidyalavExamDetailsPresenter,
                                                                                    StartBidyalavExamInteractor.Callback,
                                                                                    SubmitBidyalavAnswerInteractor.Callback,
                                                                                    EndBidyalavExamInteractor.Callback
{

    Context mContext;
    BidyalavExamDetailsPresenter.View mView;
    StartBidyalavExamInteratorImpl startExamInteractor;
    SubmitBidyalavAnswerInteractorImpl submitAnswerInteractor;
    EndBidyalavExamInteractorImpl endExamInteractor;

    public BidyalavExamDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void startExam(int examId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.error(mContext, "Session Expired !! Please Login Again").show();
        } else {
            startExamInteractor = new StartBidyalavExamInteratorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id, examId);
            startExamInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void submitAnswer(int studentExamId, int questionId, int answerId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.error(mContext, "Session Expired !! Please Login Again").show();
        } else {
            submitAnswerInteractor = new SubmitBidyalavAnswerInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, studentExamId, questionId, answerId);
            submitAnswerInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void endExam(int studentExamId, int questionId, int answerId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.error(mContext, "Session Expired !! Please Login Again").show();
        } else {
            endExamInteractor = new EndBidyalavExamInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, studentExamId, questionId, answerId);
            endExamInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingExamDetailsSuccess(ExamDetails examDetails) {
        mView.loadExamDetails(examDetails);
        mView.hideLoader();
    }

    @Override
    public void onGettingExamDetailsFail(int loginError, String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onAnswerSubmitSuccess() {
        mView.onAnswerSubmittedSuccess();
        mView.hideLoader();
    }

    @Override
    public void onAnswerSubmitFail(int loginError, String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onEndExamSuccess() {
        mView.onEndExamSuccess();
    }

    @Override
    public void onEndExamFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

}
