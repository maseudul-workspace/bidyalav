package com.project.bidyalav.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.domain.executors.Executor;
import com.project.bidyalav.domain.executors.MainThread;
import com.project.bidyalav.domain.interactors.FetchMainDataInteractor;
import com.project.bidyalav.domain.interactors.impl.FetchMainDataInteractorImpl;
import com.project.bidyalav.domain.models.MainData;
import com.project.bidyalav.domain.models.UserInfo;
import com.project.bidyalav.presentation.presenters.MainPresenter;
import com.project.bidyalav.presentation.presenters.base.AbstractPresenter;
import com.project.bidyalav.presentation.ui.adapters.ClassMainAdapter;
import com.project.bidyalav.presentation.ui.adapters.OrganizationHorizontalAdapter;
import com.project.bidyalav.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter,
                                                                    FetchMainDataInteractor.Callback,
                                                                    ClassMainAdapter.Callback,
                                                                    OrganizationHorizontalAdapter.Callback
{

    Context mContext;
    MainPresenter.View mView;
    FetchMainDataInteractorImpl fetchMainDataInteractor;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchMainData() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.error(mContext, "Session Expired !! Please Login Again").show();
        } else {
            fetchMainDataInteractor = new FetchMainDataInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.id);
            fetchMainDataInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingMainDataSuccess(MainData mainData) {
        ClassMainAdapter classMainAdapter = new ClassMainAdapter(mContext, mainData.classes, this);
        OrganizationHorizontalAdapter organizationHorizontalAdapter = new OrganizationHorizontalAdapter(mContext, mainData.organizations, this);
        mView.loadData(mainData.orgImage, classMainAdapter, organizationHorizontalAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingMainDataFailed(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onSubjectClicked(int subjectId) {
        mView.goToSubjectDetails(subjectId);
    }
}
