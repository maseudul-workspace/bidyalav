package com.project.bidyalav.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.bidyalav.R;
import com.project.bidyalav.domain.models.Class;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ClassMainAdapter extends RecyclerView.Adapter<ClassMainAdapter.ViewHolder> implements SubjectMainAdapter.Callback {

    public interface Callback {
        void onSubjectClicked(int subjectId);
    }

    Context mContext;
    Class[] classes;
    Callback mCallback;

    public ClassMainAdapter(Context mContext, Class[] classes, Callback callback) {
        this.mContext = mContext;
        this.classes = classes;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_class_main, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewClass.setText(classes[position].name);
        SubjectMainAdapter adapter = new SubjectMainAdapter(mContext, classes[position].subjects, this);
        holder.recyclerViewSubjects.setAdapter(adapter);
        holder.recyclerViewSubjects.setLayoutManager(new GridLayoutManager(mContext, 3));
        holder.classHeaderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.recyclerViewSubjects.getVisibility() == View.VISIBLE) {
                    holder.imgViewArrowDown.animate().setDuration(400).rotation(180).start();
                    holder.recyclerViewSubjects.setVisibility(View.GONE);
                } else {
                    holder.imgViewArrowDown.animate().setDuration(400).rotation(0).start();
                    holder.recyclerViewSubjects.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return classes.length;
    }

    @Override
    public void onSubjectClicked(int id) {
        mCallback.onSubjectClicked(id);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_class)
        TextView txtViewClass;
        @BindView(R.id.recycler_view_subjects)
        RecyclerView recyclerViewSubjects;
        @BindView(R.id.img_view_arrow_down)
        ImageView imgViewArrowDown;
        @BindView(R.id.class_header_layout)
        View classHeaderLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
