package com.project.bidyalav.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.project.bidyalav.AndroidApplication;
import com.project.bidyalav.domain.models.NotificationModel;
import com.project.bidyalav.util.NotificationUtils;

public class AppFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        String message = "";
        try{
            message = remoteMessage.getData().get("message");
        }catch (NullPointerException e){
            message = "From ITC Application";
        }

        String title;
        try{
            title = remoteMessage.getData().get("title");
        }catch (NullPointerException e){
            title = "Notification Title";
        }

        NotificationModel notificationModel = new NotificationModel(title, message);
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
//        if(androidApplication.getUserInfo(this) != null && androidApplication.getUserInfo(this).api_key.equals(apiKey)){
//            notificationUtils.displayNotification(notificationModel, resultIntent);
//        }
    }

    @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent){
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartServicePendingIntent);

        super.onTaskRemoved(rootIntent);
    }

}
